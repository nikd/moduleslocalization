# Automating the Incremental  Evolution of Controllers for Physical Robots #


Evolutionary robotics is challenged with some key issues that must
 be solved, or at least mitigated extensively, before it can fulfill some of its  promises to deliver highly autonomous and adaptive robots. The \emph{reality gap} and the ability to transfer phenotypes from simulation to reality is one such problem. Another lies in the embodiment of the evolutionary processes which  links to the first, but focuses on how evolution can act on real agents and occur independent from simulation i.e.\ going from being ``the evolution of things, rather than just the evolution of digital objects...''. The work presented here investigates how fully autonomous evolution of robot controllers can be realized in hardware using an industrial robot and a marker-based computer vision system.


In particular, this system presents an approach to automate the reconfiguration of the test environment and shows that it is possible, for the first time, to incrementally evolve a neural robot controller for different  obstacle avoidance task with \emph{no human intervention}. Importantly, the system offers a high level of robustness and precision that could potentially open up the range of problems amendable to embodied evolution.

Cite:
[Andres Faina, Lars Toft Jacobsen and Sebastian Risi, "Automating the Incremental Evolution of Controllers for Physical Robots", Special Issue on the Evolution of Physical Systems, Artificial Life, 2017](https://real.itu.dk/wp-content/uploads/sites/11/2017/09/artl_a_00226.pdf)