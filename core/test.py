import vision
import numpy as np
import cv2
from collections import deque

# Setup video capture to 640x480
cap = cv2.VideoCapture(0)
cap.set(3, 1280)
cap.set(4, 720)

# trace = deque([])
# trace2 = deque([])
capture = False
while(True):
    ret, frame = cap.read()
    if capture:
        cv2.imwrite('raw.jpg', frame)
        markers = vision.get_markers(cam=False,frame=frame, capture=True)
    else:
        markers = vision.get_markers(cam=False,frame=frame)

    #
    # Visual output

    for m in markers:
        center = m.center()
        center = (int(center[0]), int(center[1]))
        # if m.mid == 1:
        #     trace.append(center)
        # if m.mid == 12:
        #     trace2.append(center)
        angle = m.orientation()
        # cv2.putText(frame, str(m.mid) + " angle: " + str(int(angle)-90), center, cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255))
        cv2.putText(frame, str(m.mid), center, cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,0,255), 2)
        # cv2.drawContours(frame, [m.root], -1, (255,0,0), 1)
        # cv2.circle(frame, center, 4, (255,0,0),1)
        # if m.mid == 5:


    # if len(trace) > 100:
    #     trace.popleft()

    # if len(trace2) > 100:
    #     trace2.popleft()

    # for i in range(1, len(trace)):
    #     cv2.line(frame, trace[i-1], trace[i], (0,0,255), 1)

    # for i in range(1, len(trace2)):
    #     cv2.line(frame, trace2[i-1], trace2[i], (0,255,0), 1)

    cv2.imshow('live', frame)
    # cv2.imshow('thresh', thresh)
    # cv2.imshow('thresh2', th2)
    capture = False
    key = cv2.waitKey(1)
    if key & 0xFF == ord('c'):
        capture = True
    elif key & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
