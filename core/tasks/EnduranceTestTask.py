import vision
import serial
import time
import random
import ur5
import MultiNEAT as NEAT
from math import pi, sqrt, fabs
import logging
from multiprocessing import Process, Queue
import pickle
import cv2
from Task import Task

class EnduranceTestTask(Task):
    """Type #1. This task moves an object around to random positions
    while testing the accuracy of the CV system and the endurance
    of the complete set-up. Should be run for MANY iterations."""

    object_id = 5
    object_positions = {object_id: ((0.5, 0.0), 0)}

    def __init__(self, robot=None, iterations=1000):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        fh = logging.FileHandler('output/exp1/experiments.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        self.rob = robot
        self.iterations = iterations      
        self.detect_errors = 0
        self.detections = 0
        self.pick_errors = 0
        
    def setup(self):
        if self.rob is None:
            self.rob = ur5.load()
        self.rob.home()
        #wait until the trabsform is calcualted
        marker = vision.get_marker_object(1)
        while(marker.realxy() is None):
            marker = vision.get_marker_object(1)
        print "Done"
        #self.rob.build([marker], self.object_positions.copy())


    def run(self):
        self.logger.info('<< Experiment type #1 started >>')
        self.setup()
        f = open('output/exp1/exp_type1_plot.csv', 'a', 1)
        f.write('place_x,place_y,place_o,vision_x,vision_y,vision_o\n')
        self.logger.info("Writing values to exp_type1_plot.csv")
        m = vision.get_marker_object(self.object_id)
        
        pos = []
#        pos.append((0.65, -0.40, pi/2))
#        pos.append((0.35, -0.40, pi/2))
#        pos.append((0.65, 0.0, pi/2))
#        pos.append((0.35, 0.0, pi/2))
#        pos.append((0.65, 0.40, pi/2))
#        pos.append((0.35, 0.40, pi/2))
        
        pos.append((0.5, 0.0, 0))
        pos.append((0.5, 0.0, -pi/4))
        pos.append((0.5, 0.0, -pi/2))
        pos.append((0.5, 0.0, -3*pi/4))
        pos.append((0.5, 0.0, -pi))
        pos.append((0.5, 0.0, -5*pi/4))
        pos.append((0.5, 0.0, -6*pi/4))
        pos.append((0.5, 0.0, -7*pi/4))
        pos.append((0.5, 0.0, -8*pi/4))
        
        
        for i in range(self.iterations):
            if i % 25 == 0:
                self.logger.info('Total images processed: %d, detection errors: %d',
                                 self.detections, self.detect_errors)
            if(i%3 == 0):
                vision.calibrate()
            x = random.uniform(0.35, 0.65)
            y = random.uniform(-0.4, 0.4)
            o = random.uniform(0.0, 2*pi)
            iterPos = i%len(pos) 
            o = pos[iterPos][2]
            x, y = pos[iterPos][0], pos[iterPos][1]
            self.rob.pick_and_place(m, (x, y), o)
            self.rob.home()
            m = vision.get_marker_object(self.object_id)
            rxy = m.realxy()
            ang = m.orientation()
            dist_to_target = vision.euclidian_distance(rxy[:2], (x,y))
            if dist_to_target > 0.05:
                self.pick_errors += 1
            f.write(str(x)+','+str(y)+','+str(o)+','+str(rxy[0])+','+str(rxy[1])+','+str(ang)+'\n')
            print "Iter " + str(i)
        f.close()
        self.rob.cleanup()
        self.rob = None
        self.logger.info('Total images processed: %d, detection errors: %d', self.detections, self.detect_errors)
        self.logger.info('Pick errors: %d', self.pick_errors)
        self.logger.info('-- Experiment type #1 ended --')