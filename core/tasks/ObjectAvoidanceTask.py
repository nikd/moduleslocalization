import vision
import serial
import time
import random
import ur5
import MultiNEAT as NEAT
from math import pi, sqrt, fabs, degrees
import logging
from multiprocessing import Process, Queue
import pickle
import cv2
from Task import Task, normalize, constrain
import numpy as np
import matplotlib.pyplot as plt

class ObjectAvoidanceTask(Task):
    """Type #3. Evolves a controller with the objective of going
    from an edge position to a position opposite the arena.
    It should avoid two objects placed in the environment as it finds
    its way towards the target (virtual).
    """

    zumo_id = [5, 6]
    o_ids = [7, 8, 9, 10]
#    configurations = [{
#        'zumo_position': ((0.65, 0.35), 3*(pi/2)),
#        'object_positions': {o_ids[0]: ((0.4, 0.35), 0.0),
#                            o_ids[1]: ((0.54, 0.0), 0.0)},
#        'goal_position': (0.4, -0.4)},
#        {
#        'zumo_position': ((0.65, -0.35), 3*(pi/2)),
#        'object_positions': {o_ids[0]: ((0.4, -0.35), 0.0),
#                            o_ids[1]: ((0.54, 0.0), 0.0)},
#        'goal_position': (0.4, 0.4)}]
    configurations = [{
        'zumo_position': ((0.50, 0.35), 2 *(pi/2)),
        'object_positions': {o_ids[0]: ((0.67, 0.20), 0.0),
                            o_ids[1]: ((0.47, 0.00), 0.0),
                            o_ids[2]: ((0.70, -0.17), 0.0),
                            o_ids[3]: ((0.37, 0.10), 0.0),
                            },
        'goal_position': (0.50, -0.35)},
        {
        'zumo_position': ((0.50, 0.35), 2*(pi/2)),
        'object_positions': {o_ids[0]: ((0.33, 0.20), 0.0),
                            o_ids[1]: ((0.53, 0.00), 0.0),
                            o_ids[2]: ((0.30, -0.17), 0.0),
                            o_ids[3]: ((0.63, 0.10), 0.0),
                            },
        'goal_position': (0.50, -0.35)}]
    easyConfigurations = [{
        'zumo_position': ((0.50, 0.35), 2 *(pi/2)),
        'object_positions': {o_ids[0]:((0.67, 0.16), 0.0),
                            o_ids[1]: ((0.60, 0.08), 0.0),
                            o_ids[2]: ((0.53, 0.00), 0.0),
                            o_ids[3]: ((0.60, -0.10), 0.0),
                            },
        'goal_position': (0.50, -0.35)},
        {
        'zumo_position': ((0.50, 0.35), 2*(pi/2)),
        'object_positions': {o_ids[0]:((0.33, 0.16), 0.0),
                            o_ids[1]: ((0.40, 0.08), 0.0),
                            o_ids[2]: ((0.47, 0.00), 0.0),
                            o_ids[3]: ((0.40, -0.10), 0.0),
                            },
        'goal_position': (0.50, -0.35)}]
    steps = 40
    def __init__(self, robot=None, population_size=15, generations=20, resume=False):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        fh = logging.FileHandler('output/exp3/experiments.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        if robot is None:
            self.rob = ur5.load()
        else:
            self.rob = robot
        self.resume = resume
        self.population_size = population_size
        self.generations = generations
        self.ser = serial.Serial()
        self._setupSerial()
        self._setupNEAT(self.population_size)
        self.start_gen = 0
        self.relax_dist = 0.012
        self.relax_angle = 0.15
        self.fig = plt.figure(figsize = (12,12))
        self.graphFig = self.fig.add_subplot(1, 1, 1)
        if resume:
            g_file = open('output/exp3/current_gen', 'r')
            self.start_gen = pickle.load(g_file)
            g_file.close()
                #wait until the trabsform is calcualted
        marker = vision.get_marker_object(1, self.rob)
        while(marker.realxy() is None):
            marker = vision.get_marker_object(1, self.rob)
            
        self.incremental = True
        self.incrementalChange = 3
        
        print "Done"
    
    def getConf(self):
        if self.incremental:
            return self.easyConfigurations
        else:
            return self.configurations
        
    def setup(self, conf=0):
        config = self.getConf()
        o_positions = config[conf]['object_positions']
        z_position = {self.zumo_id[0]: config[conf]['zumo_position']}
        
        #check that the zumo in its starting place
        in_place = self.zumo_in_place(conf)
        while not in_place:
            self.rob.build(z_position, relax_dist=self.relax_dist, relax_angle=self.relax_angle)
            in_place = self.zumo_in_place(conf)

        in_place = self.all_in_place(conf)
        while not in_place:
            # self.rob.build(z_position, relax_dist=0.03, relax_angle=0.6)
            # if in_place:
            #     break
            self.rob.build(o_positions.copy(), relax_dist=self.relax_dist, relax_angle=self.relax_angle)
            in_place = self.all_in_place(conf)
            
    def zumo_in_place(self, conf=0):
        config = self.getConf()
        zumo_pos = config[conf]['zumo_position']
        in_place = True
        m = vision.get_marker_object_fast(self.zumo_id[0], self.rob)
        real_zumo_pos = m.realxy()[:2]
#        print "zumo_pos: ", real_zumo_pos, "zumo_goal_pos: ", zumo_pos[0]
        dist = vision.euclidian_distance(real_zumo_pos, zumo_pos[0])
        angle = fabs(m.orientation() - zumo_pos[1])
        if angle > pi:
            angle -= 2.0*pi
            angle = fabs(angle)
        if dist > self.relax_dist or angle > self.relax_angle:
            in_place = False
        
        return in_place
    
    def all_in_place(self, conf=0):
#        print "all_in_place function"
        config = self.getConf()
        zumo_pos = config[conf]['zumo_position']
        
        in_place = True
        # m = vision.get_marker_object(self.zumo_id)
        # dist = vision.euclidian_distance(m.realxy()[:2], zumo_pos[0])
        # rot = fabs(m.orientation() - zumo_pos[1])
        # if dist > 0.1 or rot > pi/2:
        #     in_place = False
        obj_pos = config[conf]['object_positions']
        for i,p in obj_pos.iteritems():
            m = vision.get_marker_object(i, self.rob)
            dist = vision.euclidian_distance(m.realxy()[:2], p[0])
            rot = fabs(m.orientation() - p[1])
            if dist > self.relax_dist : #or rot > 0.3
                return False
        return in_place

    def _setupSerial(self):
        self.ser = serial.Serial()
        self.ser.baudrate = 9600
        self.ser.timeout = 1
        self.ser.port = 'COM22'

    def _setupNEAT(self, pop_size):
        """Setup NEAT population and network.
        2 inputs: normalized heading + bias
        2 outputs: normalized motor outputs
         """
        if self.resume:
            print "loading population"
            self.population = NEAT.Population('output/exp3/progress')
            print "next epoch"
            self.population.Epoch()
        else:
            params = NEAT.Parameters()
            params.PopulationSize = pop_size
            params.MinSpecies = 1
            params.MaxSpecies = 3
            params.SpeciesDropoffAge = 3
            params.YoungAgeThreshold = 2
            params.OldAgeThreshold = 5
            params.OverallMutationRate = 0.8
            genome = NEAT.Genome(
                0, 4, 0, 2, False, NEAT.ActivationFunction.UNSIGNED_SIGMOID,
                NEAT.ActivationFunction.UNSIGNED_SIGMOID, 0, params)
            self.population = NEAT.Population(genome, params, True, 1.0, random.randint(0,1000000))

    def evaluate(self, genome, conf):
        net = NEAT.NeuralNetwork()
        genome.BuildPhenotype(net)
        
        self.logger.info('Genome ' + str(genome.GetID())+': Running evaluation '+str(conf+1)+' of '+str(len(self.configurations)))
        positions = []
        sensorL = []
        sensorR = []
        action = []
        config = self.getConf()
        goal = config[conf]['goal_position']
        obj_pos = config[conf]['object_positions']
        self.ser.write('R\n')
        time.sleep(0.5)
        self.ser.write('S\n')
        time.sleep(0.1)
        col = False
        frames = 0
        
        obj1 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[0], self.rob)
        obj2 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[1], self.rob)
        obj3 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[2], self.rob)
        obj4 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[3], self.rob)
        rxy1_original = obj1.realxy()
        rxy2_original = obj2.realxy()
        rxy3_original = obj3.realxy()
        rxy4_original = obj4.realxy()
                
        while frames < ObjectAvoidanceTask.steps:
            self.ser.write('W\n') 
            time.sleep(.4)
            line = self.ser.readline()
            if line.startswith('I'):
                frames += 1
                zumoMarker = vision.get_marker_object_fast(ObjectAvoidanceTask.zumo_id[0], self.rob)
                rxy = zumoMarker.realxy()
                orient = zumoMarker.orientation()
                positions.append(rxy)
                pp_angle = vision.Marker.angle_between_points(rxy[1], rxy[0], goal[1], goal[0])
                r_angle = self.relative_angle(orient, pp_angle)
                n_angle = normalize(r_angle, -pi, pi)
                inputs = [n_angle]
                in_data = line.strip().split(' ')
                # parse, normalize and add prox. sensor values to inputs
                # for i in range(3, 9):
                #     inputs.append(normalize(int(in_data[i]), 0, 6))
                #FAI inputs.append(normalize(int(in_data[3])+int(in_data[4]), 0, 12))
                #FAI inputs.append(normalize(int(in_data[5])+int(in_data[6]), 0, 12))
                #FAI inputs.append(normalize(int(in_data[7])+int(in_data[8]), 0, 12))
                inputs.append(normalize(int(in_data[5]), 0, 10))
                inputs.append(normalize(int(in_data[6]), 0, 10))
                inputs.append(1.0)
                net.Input(inputs) # angle, 3 x sensors, bias
                net.Activate()
                output = net.Output()
                if output[0] < 0.4:
                    speed = str(Task.turn_speed)
                    delay = str(Task.turn_time)
                    cmd = 'T,'+in_data[1]+',0,'+speed+','+delay+'\n'
                    a = "left"
                elif output[0] > 0.6:
                    speed = str(Task.turn_speed)
                    delay = str(Task.turn_time)
                    cmd = 'T,'+in_data[1]+',1,'+speed+','+delay+'\n'
                    a = "right"
                else:
                    speed = str(Task.fwd_speed)
                    delay = str(Task.fwd_time)
                    cmd = 'F,'+in_data[1]+','+speed+','+delay+'\n'
                    a = "straight"

#                #Test best controller
#                if(int(in_data[6]) > 0):
#                    speed = str(Task.turn_speed)
#                    delay = str(Task.turn_time)
#                    cmd = 'T,'+in_data[1]+',0,'+speed+','+delay+'\n'
#                    a = "left"
#                elif(int(in_data[5]) > 0):
#                    speed = str(Task.turn_speed)
#                    delay = str(Task.turn_time)
#                    cmd = 'T,'+in_data[1]+',1,'+speed+','+delay+'\n'
#                    a = "right"
#                else:
#                    speed = str(Task.fwd_speed)
#                    delay = str(Task.fwd_time)
#                    cmd = 'F,'+in_data[1]+','+speed+','+delay+'\n'
#                    a = "straight"

                print "inputs: ", in_data[3], "   ,", in_data[5], ", ", in_data[6], "   ,", in_data[7], "action: ", a
                self.ser.write(cmd)
                # Write stop condition
                l = len(positions)
                zumoMarker = vision.get_marker_object_fast(ObjectAvoidanceTask.zumo_id[0], self.rob)
                rxy = zumoMarker.realxy()
                orient = zumoMarker.orientation()
#                    print "rxy: ", rxy[:2]
                action.append(output[0])
                sensorL.append(in_data[5])
                sensorR.append(in_data[6])
#                    if l >= 5:
#                        delta_x = abs(positions[l-1][0]-positions[l-5][0])
#                        delta_y = abs(positions[l-1][1]-positions[l-5][1])
#                        if delta_x + delta_y < 0.001:
#                            self.logger.warning('Premature end of evaluation - stasis')
#                            break
                obj1 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[0], self.rob)
                obj2 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[1], self.rob)
                obj3 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[2], self.rob)
                obj4 = vision.get_marker_object_fast(ObjectAvoidanceTask.o_ids[3], self.rob)
                rxy1 = obj1.realxy()
                rxy2 = obj2.realxy()
                rxy3 = obj3.realxy()
                rxy4 = obj4.realxy()
                dist1 = vision.euclidian_distance(rxy1[:2], rxy1_original[:2])
                dist2 = vision.euclidian_distance(rxy2[:2], rxy2_original[:2])
                dist3 = vision.euclidian_distance(rxy3[:2], rxy3_original[:2])
                dist4 = vision.euclidian_distance(rxy4[:2], rxy4_original[:2])
                # object collision
                if dist1 > 0.005 or dist2 > 0.005 or dist3 > 0.005 or dist4 > 0.005:
                    self.logger.warning('Premature end of evaluation - collision')
#                            print "dist1: ", dist1, "rxy: ", rxy[:2], "obsPos: ", obj_pos[ObjectAvoidanceTask.o_ids[0]][0]
#                            print "dist1: ", dist2, "rxy: ", rxy[:2], "obsPos: ", obj_pos[ObjectAvoidanceTask.o_ids[1]][0]
                    col = True
                    break

        time.sleep(0.8)
        self.ser.write('R\n')
        fitness = 1-constrain(vision.euclidian_distance(rxy[:2], goal), 0.0, 1.0)

        self.logger.info('Fitness obtained: '+str(fitness))
        return (fitness, list(positions), list(sensorL), list(sensorR), list(action))
    
    def calculate_fitness(self, positions, goal, steps, collision):
        ''' Fitness is the sum of the distances from robot to target
        over the steps evaluated times the total displacement of the
        objects that must be avoided
        '''
        sum = 0
        for p in positions:
            dist = vision.euclidian_distance(p[:2], goal)
            fit_dist = interval_map(constrain(dist, 0.0, 0.9), 0.0, 0.9, 20.0, 0.0)
            # sum += pow(fit_dist, 2)
            sum += fit_dist
        if collision:
            return ((1.0/steps)*sum)-1.00
        else:
            return (1.0/steps)*sum

    def run(self):
        fitAvgArray = []
        fitBestArray = []
        self.logger.info('<< Experiment type #3 started >>')
        self.rob.home()
        mode = 'a' if self.resume else 'w'
        f = open('output/exp3/exp_type3_plot.csv', mode, 1)
        if not self.resume:
            print "resumming the evolution"
            f.write('generation,id,fitness\n')
            f.flush()
        self.setup()
        # open serial connection
        self.ser.open()
        for gen in range(self.start_gen, self.generations):
            
            if (gen >= self.incrementalChange):
                self.incremental = False
  
            
            vision.calibrate()
            self.logger.info("GENERATION: " + str(gen))
            # retrieve and evaluate all genomes
            genome_list = NEAT.GetGenomeList(self.population)
            print "Genome list size: ", len(genome_list)
            configs = len(self.configurations)
            fitnessArray = np.zeros(len(genome_list))
            paths = [None]*len(genome_list)
            totalSensorR = [None]*len(genome_list)
            totalSensorL = [None]*len(genome_list)
            totalActions = [None]*len(genome_list)
            for i in range(len(genome_list)):
                totalSensorR[i]=[]
                totalSensorL[i]=[]
                totalActions[i]=[]
                paths[i]=[]
            for conf in range(configs):
                index = 0
                for genome in genome_list:
                    self.setup(conf) # reset environment
                    fitness, positions, leftSensorValues, rightSensorValues, actions = self.evaluate(genome, conf)
                    fitnessArray[index] += fitness
                    paths[index].append(positions)
                    totalSensorL[index].append(leftSensorValues)
                    totalSensorR[index].append(rightSensorValues)
                    totalActions[index].append(actions)
                    index +=1
            
            #Calculate statistics and assign fitness to genomes:
            index = 0
            fitness_sum = 0.0
            fitness_best = 0.0
            for genome in genome_list:  
                fitness = fitnessArray[index] / float(configs)
                fitness_sum += fitness
                if fitness > fitness_best:
                    fitness_best = fitness
                    positions_best = paths[index]
                    leftSensorValues_best = totalSensorL[index]
                    rightSensorValues_best = totalSensorR[index]
                    actions_best = totalActions[index]
                genome.SetFitness(fitness)
                f.write(str(gen)+','+str(genome.GetID())+','+str(fitness)+'\n')
                f.flush()
                self.logger.info("Genome: %d Fitness: %f", genome.GetID(), fitness)    
                index += 1
                
            # advance to next generation
            p_file = open('output/exp3/best_positions_'+str(gen), 'w')
            pickle.dump(positions_best, p_file)
            p_file.close()
            sl_file = open('output/exp3/best_leftsensor_'+str(gen), 'w')
            pickle.dump(leftSensorValues_best, sl_file)
            sl_file.close()
            sr_file = open('output/exp3/best_rightsensor_'+str(gen), 'w')
            pickle.dump(rightSensorValues_best, sr_file)
            sr_file.close()
            a_file = open('output/exp3/best_actions_'+str(gen), 'w')
            pickle.dump(actions_best, a_file)
            a_file.close()
            self.population.Save('output/exp3/progress')
            g_file = open('output/exp3/current_gen', 'w')
            pickle.dump(gen+1, g_file)
            g_file.close()
            avg_fitness = fitness_sum/len(genome_list)
            fitAvgArray.append(avg_fitness)
            fitBestArray.append(fitness_best)
            self.graphFig.plot(range(gen+1), fitAvgArray, color='green', linewidth=1)
            self.graphFig.plot(range(gen+1), fitBestArray, color='red', linewidth=1)
            plt.savefig('output/exp3/evolution')
            self.logger.info('Avg. fitness: %f, Best fitness: %f', avg_fitness, fitness_best)
            
            self.logger.info("Advancing to next generation...")
            self.population.Epoch()
        # Close up files and serial
        cv2.destroyAllWindows()
        self.logger.info('-- Experiment type #3 ended --')
        self.ser.close()
        f.close()
        self.rob.cleanup()
        