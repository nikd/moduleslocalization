import vision
import serial
import time
import random
import ur5
import MultiNEAT as NEAT
from math import pi, sqrt, fabs
import logging
from multiprocessing import Process, Queue
import pickle
import cv2
from Task import Task
        
class DummyTask(Task):
    """Type #4. Just a test to see how the evolution works
    """

    zumo_id = [5, 6]
    o_ids = [8, 9]

    configurations = [{
        'zumo_position': ((0.50, 0.35), 2*(pi/2)),
        'object_positions': {o_ids[0]: ((0.45, 0.00), 0.0),
                            o_ids[1]: ((0.68, 0.00), 0.0),
                            },
        'goal_position': (0.50, -0.4)},
        {
        'zumo_position': ((0.50, 0.35), 2*(pi/2)),
        'object_positions': {o_ids[0]: ((0.55, 0.00), 0.0),
                            o_ids[1]: ((0.32, 0.00), 0.0),
                            },
        'goal_position': (0.50, -0.4)}]
    steps = 5
    fwd_speed = 120
    turn_speed = 100
    fwd_time = 180
    turn_time = 100

    def __init__(self, robot=None, population_size=15, generations=20, resume=False):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        fh = logging.FileHandler('output/exp4/experiments.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        if robot is None:
            self.rob = ur5.load()
        else:
            self.rob = robot
        self.resume = resume
        self.population_size = population_size
        self.generations = generations
        self.ser = serial.Serial()
        self.transform, _ = vision.calibration.restore()
        self.tracker = vision.Tracker(ObjectAvoidanceTask.zumo_id[0], self.transform, mid_aux = ObjectAvoidanceTask.zumo_id[1])
        self._setupSerial()
        self._setupNEAT(self.population_size)
        self.start_gen = 0
        if resume:
            g_file = open('output/exp4/current_gen', 'r')
            self.start_gen = pickle.load(g_file)
            g_file.close()

    def setup(self, conf=0):
        o_positions = self.configurations[conf]['object_positions']
        z_position = {self.zumo_id[0]: self.configurations[conf]['zumo_position']}
        
        

    
    
    

    def _setupSerial(self):
        self.ser = serial.Serial()
        self.ser.baudrate = 9600
        self.ser.timeout = 1
        self.ser.port = 'COM21'

    def _setupNEAT(self, pop_size):
        """Setup NEAT population and network.
        2 inputs: normalized heading + bias
        2 outputs: normalized motor outputs
         """
        if self.resume:
            self.population = NEAT.Population('output/exp4/progress')
            self.population.Epoch()
        else:
            params = NEAT.Parameters()
            params.PopulationSize = pop_size
            params.MinSpecies = 1
            params.MaxSpecies = 3
            params.SpeciesDropoffAge = 3
            params.YoungAgeThreshold = 2
            params.OldAgeThreshold = 5
            params.OverallMutationRate = 0.8
            genome = NEAT.Genome(
                0, 4, 0, 2, False, NEAT.ActivationFunction.UNSIGNED_SIGMOID,
                NEAT.ActivationFunction.UNSIGNED_SIGMOID, 0, params)
            self.population = NEAT.Population(genome, params, True, 1.0, random.randint(0,1000000))

    def evaluate(self, genome):
        net = NEAT.NeuralNetwork()
        genome.BuildPhenotype(net)
        fitness = 0.0
        paths = []
        totalSensorR = []
        totalSensorL = []
        totalActions = []
        configs = len(self.configurations)
        for i in range(configs):
            self.logger.info('Genome ' + str(genome.GetID())+': Running evaluation '+str(i+1)+' of '+str(configs))
            positions = []
            sensorL = []
            sensorR = []
            action = []
            self.tracker.start()
            goal = self.configurations[i]['goal_position']
            obj_pos = self.configurations[i]['object_positions']
            self.ser.write('R\n')
            time.sleep(0.5)
            self.ser.write('S\n')
            time.sleep(0.1)
            col = False
            frames = 0
            while frames < DummyTask.steps:
                line = self.ser.readline()
                if line.startswith('I'):
                    frames += 1
#                    rm = self.tracker.getMarker()
#                    while rm is None:
#                        self.logger.warning('Track frame miss')
#                        rm = self.tracker.getMarker()
#                    rxy = rm.realxy()
#                    orient = rm.orientation()
                    rxy, orient = self.tracker.getPosition()
                    positions.append(rxy)
                    pp_angle = vision.Marker.angle_between_points(rxy[1], rxy[0], goal[1], goal[0])
                    r_angle = self.relative_angle(orient, pp_angle)
                    n_angle = normalize(r_angle, -pi, pi)
                    inputs = [n_angle]
                    in_data = line.strip().split(' ')
                    # parse, normalize and add prox. sensor values to inputs
                    # for i in range(3, 9):
                    #     inputs.append(normalize(int(in_data[i]), 0, 6))
                    #FAI inputs.append(normalize(int(in_data[3])+int(in_data[4]), 0, 12))
                    #FAI inputs.append(normalize(int(in_data[5])+int(in_data[6]), 0, 12))
                    #FAI inputs.append(normalize(int(in_data[7])+int(in_data[8]), 0, 12))
                    inputs.append(normalize(int(in_data[5]), 0, 10))
                    inputs.append(normalize(int(in_data[6]), 0, 10))
                    inputs.append(1.0)
                    net.Input(inputs) # angle, 3 x sensors, bias
                    net.Activate()
                    output = net.Output()
                    if output[0] < 0.4:
                        speed = str(StraightToTargetTask.turn_speed)
                        delay = str(StraightToTargetTask.turn_time)
                        cmd = 'T,'+in_data[1]+',0,'+speed+','+delay+'\n'
                    elif output[0] > 0.6:
                        speed = str(StraightToTargetTask.turn_speed)
                        delay = str(StraightToTargetTask.turn_time)
                        cmd = 'T,'+in_data[1]+',1,'+speed+','+delay+'\n'
                    else:
                        speed = str(StraightToTargetTask.fwd_speed)
                        delay = str(StraightToTargetTask.fwd_time)
                        cmd = 'F,'+in_data[1]+','+speed+','+delay+'\n'
                    self.ser.write(cmd)
                    # Write stop condition
                    l = len(positions)
                    rxy, orient = self.tracker.getPosition()
#                    print "rxy: ", rxy[:2]
                    print "output: ", output[0]
                    print "in_data[5]: ", in_data[5]
                    action.append(output[0])
                    sensorL.append(in_data[5])
                    sensorR.append(in_data[6])
                    if l >= 5:
                        delta_x = abs(positions[l-1][0]-positions[l-5][0])
                        delta_y = abs(positions[l-1][1]-positions[l-5][1])
                        if delta_x + delta_y < 0.0002:
                            self.logger.warning('Premature end of evaluation - stasis')
                            break
                        dist1 = vision.euclidian_distance(rxy[:2], obj_pos[DummyTask.o_ids[0]][0])
                        dist2 = vision.euclidian_distance(rxy[:2], obj_pos[DummyTask.o_ids[1]][0])
                        # object collision
                        if dist1 < 0.09 or dist2 < 0.09:
                            self.logger.warning('Premature end of evaluation - collision')
#                            print "dist1: ", dist1, "rxy: ", rxy[:2], "obsPos: ", obj_pos[ObjectAvoidanceTask.o_ids[0]][0]
#                            print "dist1: ", dist2, "rxy: ", rxy[:2], "obsPos: ", obj_pos[ObjectAvoidanceTask.o_ids[1]][0]
                            col = True
                            break
            self.tracker.stop()
            time.sleep(0.1)
            self.ser.write('R\n')
            fit = self.calculate_fitness(positions, goal, frames, col)
            fitness += fit
            self.logger.info('Fitness obtained: '+str(fit))
            totalSensorL.append(sensorL)
            totalSensorR.append(sensorR)
            totalActions.append(action)
            
            paths.append(positions)
            if configs > 1 and i < configs-1:
                self.setup(i+1)
        return (fitness/configs, list(paths), list(totalSensorL), list(totalSensorR), list(totalActions))

    def calculate_fitness(self, positions, goal, steps, collision):
        ''' Fitness is the sum of the distances from robot to target
        over the steps evaluated times the total displacement of the
        objects that must be avoided
        '''
        sum = 0
        for p in positions:
            dist = vision.euclidian_distance(p[:2], goal)
            fit_dist = interval_map(constrain(dist, 0.0, 0.9), 0.0, 0.9, 20.0, 0.0)
            # sum += pow(fit_dist, 2)
            sum += fit_dist
        if collision:
            return ((1.0/steps)*sum)-2.0
        else:
            return (1.0/steps)*sum

    def run(self):
        self.logger.info('<< Experiment type #4 started >>')
        self.rob.home()
        mode = 'a' if self.resume else 'w'
        f = open('output/exp4/exp_type4_plot.csv', mode, 1)
        if not self.resume:
            f.write('generation,id,fitness\n')
            f.flush()
        self.setup()
        # open serial connection
        self.ser.open()
        for gen in range(self.start_gen, self.generations):
            self.logger.info("GENERATION: " + str(gen))
            # retrieve and evaluate all genomes
            genome_list = NEAT.GetGenomeList(self.population)
            fitness_sum = 0.0
            fitness_best = 0.0
            iter = 0
            for genome in genome_list:
                self.logger.info("Iter: " + str(iter))
                print "genome_list size: ", len(genome_list)
                fitness, positions, leftSensorValues, rightSensorValues, actions = self.evaluate(genome)
                fitness_sum += fitness
                if fitness > fitness_best:
                    fitness_best = fitness
                    positions_best = positions
                    leftSensorValues_best = leftSensorValues
                    rightSensorValues_best = rightSensorValues
                    actions_best = actions
                genome.SetFitness(fitness)
                f.write(str(gen)+','+str(genome.GetID())+','+str(fitness)+'\n')
                f.flush()
                self.logger.info("Genome: %d Fitness: %f", genome.GetID(), fitness)
                self.setup() # reset environment
            # advance to next generation
            p_file = open('output/exp4/best_positions_'+str(gen), 'w')
            pickle.dump(positions_best, p_file)
            p_file.close()
            sl_file = open('output/exp4/best_leftsensor_'+str(gen), 'w')
            pickle.dump(leftSensorValues_best, sl_file)
            sl_file.close()
            sr_file = open('output/exp4/best_rightsensor_'+str(gen), 'w')
            pickle.dump(rightSensorValues_best, sr_file)
            sr_file.close()
            a_file = open('output/exp4/best_actions_'+str(gen), 'w')
            pickle.dump(actions_best, a_file)
            a_file.close()
            self.population.Save('output/exp4/progress')
            g_file = open('output/exp4/current_gen', 'w')
            pickle.dump(gen+1, g_file)
            g_file.close()
            avg_fitness = fitness_sum/len(genome_list)
            self.logger.info('Avg. fitness: %f, Best fitness: %f', avg_fitness, fitness_best)
            self.logger.info("Advancing to next generation...")
            self.population.Epoch()
        # Close up files and serial
        cv2.destroyAllWindows()
        self.logger.info('-- Experiment type #3 ended --')
        self.ser.close()
        f.close()
        self.rob.cleanup()
