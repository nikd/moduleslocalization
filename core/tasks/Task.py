import vision
import serial
import time
import random
import ur5
import MultiNEAT as NEAT
from math import pi, sqrt, fabs
import logging
from multiprocessing import Process, Queue
import pickle
import cv2


def interval_map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def constrain(x, low, high):
    if x < low:
        return low
    elif x > high:
        return high
    else:
        return x

def normalize(x, x_min, x_max):
    return interval_map(x, x_min, x_max, 0.0, 1.0)

def denormalize(x, y_min, y_max):
    return interval_map(x, 0.0, 1.0, y_min, y_max)


class Task(object):
    fwd_speed = 130
    turn_speed = 100
    fwd_time = 120
    turn_time = 180
    def setup(self):
        """Set up the physical environment and prep the task.
        Needs to be locally implemented
        """
        raise NotImplementedError("please override setup method")

    def run(self):
        """Run the task.
        Must be locally implemented
        """
        raise NotImplementedError("please override run method")

    def evaluate(self):
        """Perform evaluation (fitness) of single individual.
        Must be locally implemented
        """
        raise NotImplementedError("please override evaluate method")

    def relative_angle(self, a_robot, a_target):
        a = a_target - a_robot
        if a < -pi:
            return a + 2*pi
        elif a >= pi:
            return a - 2*pi
        return a