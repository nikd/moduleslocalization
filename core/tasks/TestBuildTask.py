import vision
import serial
import time
import random
import ur5
import MultiNEAT as NEAT
from math import pi, sqrt, fabs
import logging
from multiprocessing import Process, Queue
import pickle
import cv2
from Task import Task

class TestBuildTask(Task):

    zumo_id = 5
    object_positions = {zumo_id: ((0.5, 0.4), pi),
                        6: ((0.3, -0.4), 0),
                        9: ((0.6, -0.4), 0),
                        11: ((0.3, 0), 0),
                        12: ((0.6, 0), 0)}

    def __init__(self, robot, reps=10):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        fh = logging.FileHandler('output/expbuild/experiments.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        self.rob = robot
        self.transform, _ = vision.calibration.restore()
        self.repetitions = reps

    def setup(self):
        markers = vision.get_markers()
        while len(markers) < 9:
            markers = vision.get_markers()
        m, lm = self.rob.build(markers, self.object_positions.copy(), relax_dist=0.04, relax_angle=0.04)
        self.logger.info('moves: '+str(m))
        self.logger.info('late moves: '+str(lm))

    def run(self):
        self.logger.info('<< Experiment build started >>')
        self.rob.home()
        for i in range(self.repetitions):
            raw_input('Place objects and press enter')
            self.logger.info('build no '+str(i+1)+' started')
            self.setup()
            self.logger.info('build no '+str(i+1)+' completed')
