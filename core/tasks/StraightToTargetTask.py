import vision
import serial
import time
import random
import ur5
import MultiNEAT as NEAT
from math import pi, sqrt, fabs
import logging
from multiprocessing import Process, Queue
import pickle
import cv2
from Task import Task, normalize, constrain, interval_map
import os

class StraightToTargetTask(Task):
    """Type #2. Evolves a controller with the objective of going
    from a center position to a corner position as fast as possible.
    The only object in the environment is the robot - desired
    end positions are virtual.
    """

    zumo_id = 5
    object_positions = {zumo_id: ((0.5, 0.0), 3*(pi/2))}
    goal_positions = [(0.3, -0.4), (0.7, 0.4)]
    #goal_positions = [(0.7, -0.4)]
    steps = 35


    def __init__(self, robot=None, population_size=15, generations=20, resume=False, folder=None):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
        if folder is None:
            self.folder = 'output/exp2/'
        else:
            self.folder = 'output/exp2/' + folder + '/'
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
        fh = logging.FileHandler(self.folder + 'experiments.log')
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        if robot is None:
            self.rob = ur5.load()
        else:
            self.rob = robot
        self.resume = resume
        self.population_size = population_size
        self.generations = generations
        self.ser = serial.Serial()
        self._setupSerial()
        self._setupNEAT(self.population_size)
        self.start_gen = 0
        self.relax_dist = 0.01
        self.relax_angle = 0.15
        if resume:
            g_file = open(self.folder + 'current_gen', 'r')
            self.start_gen = pickle.load(g_file)
            g_file.close()
        marker = vision.get_marker_object(1, self.rob)
        while(marker.realxy() is None):
            marker = vision.get_marker_object(1, self.rob)
        print "Done"

    def setup(self):
        while(self.zumo_in_place() is not True):
            self.rob.build(self.object_positions.copy(), relax_dist=self.relax_dist, relax_angle=self.relax_angle)

    def zumo_in_place(self):
        zumo_pos = self.object_positions.copy()
        
        in_place = True
        m = vision.get_marker_object_fast(self.zumo_id, self.rob)
        real_zumo_pos = m.realxy()[:2]
    #        print "zumo_pos: ", real_zumo_pos, "zumo_goal_pos: ", zumo_pos[0]
        dist = vision.euclidian_distance(real_zumo_pos, zumo_pos[self.zumo_id][0])
        rot = fabs(m.orientation() - zumo_pos[self.zumo_id][1])
        if dist > self.relax_dist or rot > self.relax_angle:
            in_place = False
        return in_place
    
    def _setupSerial(self):
        self.ser = serial.Serial()
        self.ser.baudrate = 9600 #FAI:115200
        self.ser.timeout = 1
        self.ser.port = 'COM18'#FAI:'/dev/tty.usbserial-FTFDEXW8'

    def _setupNEAT(self, pop_size):
        """Setup NEAT population and network.
        2 inputs: normalized heading + bias
        2 outputs: normalized motor outputs
        """
        if self.resume:
            self.population = NEAT.Population('output/exp2/progress')
            self.population.Epoch()
        else:
            params = NEAT.Parameters()
            params.PopulationSize = pop_size
            params.MinSpecies = 0
            params.MaxSpecies = 2
            params.YoungAgeThreshold = 2
            params.OldAgeThreshold = 5
            params.OverallMutationRate = 0.8
            genome = NEAT.Genome(
                0, 2, 0, 1, False, NEAT.ActivationFunction.UNSIGNED_SIGMOID,
                NEAT.ActivationFunction.UNSIGNED_SIGMOID, 0, params)
            self.population = NEAT.Population(genome, params, True, 1.0, random.randint(0,1000000))

    def evaluate(self, genome):
        net = NEAT.NeuralNetwork()
        genome.BuildPhenotype(net)
        fitness = 0.0
        no_evaulations = len(StraightToTargetTask.goal_positions)
        for i in range(no_evaulations):
            self.logger.info('Genome ' + str(genome.GetID())+': Running evaluation '+str(i+1)+' of '+str(no_evaulations))
            goal = StraightToTargetTask.goal_positions[i]
            positions = []
            os = []
            att = []
            frames = 0
            self.ser.write('R\n')
            time.sleep(0.5)
            self.ser.write('S\n')
            time.sleep(0.1)
            # for s in range(StraightToTargetTask.steps):
            while frames < StraightToTargetTask.steps:
                self.ser.write('W\n') 
                time.sleep(.3)
                line = self.ser.readline()
                if line.startswith('I'):
                    frames += 1
                    # Calculate angle here to feed to next network input
                    rm = vision.get_marker_object_fast(self.zumo_id, self.rob)
                    rxy = rm.realxy()
                    orient = rm.orientation()
                    positions.append(rxy)
                    pp_angle = vision.Marker.angle_between_points(rxy[1], rxy[0], goal[1], goal[0])
                    r_angle = self.relative_angle(orient, pp_angle)
                    n_angle = normalize(r_angle, -pi, pi)
                    att.append((orient, pp_angle, r_angle, n_angle))
                    in_data = line.strip().split(' ')
                    net.Input([n_angle, 1.0]) # angle + bias
                    net.Activate()
                    output = net.Output()
                    # output = [n_angle]
                    os.append(output[0])
                    if output[0] < 0.45:
                        speed = str(Task.turn_speed)
                        delay = str(Task.turn_time)
                        cmd = 'T,'+in_data[1]+',0,'+speed+','+delay+'\n'
                    elif output[0] > 0.55:
                        speed = str(Task.turn_speed)
                        delay = str(Task.turn_time)
                        cmd = 'T,'+in_data[1]+',1,'+speed+','+delay+'\n'
                    else:
                        speed = str(Task.fwd_speed)
                        delay = str(Task.fwd_time)
                        cmd = 'F,'+in_data[1]+','+speed+','+delay+'\n'
                    self.ser.write(cmd)
                    # check distance covered last 5 steps and stop if below threshold
                    l = len(positions)
                    if l >= 5:
                        delta_x = abs(positions[l-1][0]-positions[l-5][0])
                        delta_y = abs(positions[l-1][1]-positions[l-5][1])
                        if delta_x + delta_y < 0.0002:
                            self.logger.warning('Premature end of evaluation')
                            break
            time.sleep(0.1)
            self.ser.write('R\n')
            self.logger.info('Net output min '+str(min(os))+' max '+str(max(os)))
            self.logger.info('Start orient.: '+str(att[0][0])+' angle: '+str(att[0][1])+' rel.: '+str(att[0][2])+' norm.: '+str(att[0][3]))
            fit = self.calculate_fitness(positions, goal, frames)
            fitness += fit
            self.logger.info('Fitness obtained in '+str(frames)+' frames: '+str(fit))
            # run setup in between evaluations
            if no_evaulations > 1 and i < no_evaulations-1:
                time.sleep(1.5)
                self.setup()
        return (fitness/no_evaulations, list(positions))

    def calculate_fitness(self, positions, goal, steps):
        sum = 0
        for p in positions:
            dist = vision.euclidian_distance(p[:2], goal)
            fit_dist = interval_map(constrain(dist, 0.0, 0.5), 0.0, 0.5, 1.0, 0.0)
            sum += pow(fit_dist, 2)
        return (1.0/steps)*sum

    def runbest(self, name):
        self.logger.info('<< Experiment type #2 RUN-BEST started >>')
        self.rob.home()
        self.setup()
        # open serial connection
        self.ser.open()
        best_genome = self.population.GetBestGenome()
        fitness, positions = self.evaluate(best_genome)
        p_file = open(self.folder + 'best_positions_'+name, 'w')
        pickle.dump(positions, p_file)
        p_file.close()

    def run(self):
        self.logger.info('<< Experiment type #2 started >>')
        self.rob.home()
        mode = 'a' if self.resume else 'w'
        f = open(self.folder + 'exp_type2_plot.csv', mode, 1)
        exception_file = open(self.folder + 'exceptions.csv', mode, 1)
        if not self.resume:
            exception_file.write('generation,n_exceptions\n')
            exception_file.flush()
            f.write('generation,id,fitness\n')
            f.flush()
        self.setup()
        # open serial connection
        self.ser.open()
        for gen in range(self.start_gen, self.generations):
            vision.calibrate()
            self.logger.info("GENERATION: " + str(gen))
            # retrieve and evaluate all genomes
            genome_list = NEAT.GetGenomeList(self.population)
            fitness_sum = 0.0
            fitness_best = 0.0
            for genome in genome_list:
                fitness, positions = self.evaluate(genome)
                fitness_sum += fitness
                if fitness > fitness_best:
                    fitness_best = fitness
                    positions_best = positions
                genome.SetFitness(fitness)
                f.write(str(gen)+','+str(genome.GetID())+','+str(fitness)+'\n')
                f.flush()
                self.logger.info("Genome: %d Fitness: %f", genome.GetID(), fitness)
                time.sleep(1.5)
                self.setup() # reset environment
            # advance to next generation
            p_file = open(self.folder + 'best_positions_'+str(gen), 'w')
            pickle.dump(positions_best, p_file)
            p_file.close()
            self.population.Save(self.folder + 'progress')
            g_file = open(self.folder + 'current_gen', 'w')
            pickle.dump(gen+1, g_file)
            g_file.close()
            exception_file.write(str(gen)+','+ str(self.rob.getExceptionCount())+'\n')
            exception_file.flush()
            p_file.close()
            avg_fitness = fitness_sum/self.population_size
            self.logger.info('Avg. fitness: %f, Best fitness: %f', avg_fitness, fitness_best)
            self.logger.info("Advancing to next generation...")
            self.population.Epoch()
        # Close up files and serial
        self.logger.info('-- Experiment type #2 ended --')
        self.ser.close()
        f.close()
        self.rob.cleanup()