from marker import findMarkers, Marker, preprocessImage
import numpy as np
import cv2

img = cv2.imread('images/dual-marker.jpg')

th = preprocessImage(img)
thresh = th.copy()

contours, hierarchy = cv2.findContours(th, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
markers = findMarkers(contours, hierarchy, 13);


for m in markers:
    print(m.mid)
    angle = m.getOrientation()
    print(angle)
    (x, y) = m.getCenter()
    cv2.circle(img, (x, y), 2, (0,0,255), 3)

#cv2.drawContours(img, m.black_leaves, -1, (0,0,255), 2)
#cv2.drawContours(img, m.white_leaves, -1, (0,255,0), 2)
#cv2.rectangle(img, (x,y), (x+w, y+h), (100, 80, 200), 2)
cv2.imshow('contours', img)
cv2.imshow('threshold', thresh)

while(True):
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()

