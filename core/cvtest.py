import vision
import numpy as np
import cv2
from collections import deque
from math import pi, cos, sin, sqrt, fabs


def euclidian_distance(p1, p2):
    dist = sqrt(pow(fabs(p1[0] - p2[0]), 2) + pow(fabs(p1[1] - p2[1]), 2))
    return dist


def inscribed_circle(p1, p2, p3):
    a = euclidian_distance(p1, p2)
    b = euclidian_distance(p1, p3)
    c = euclidian_distance(p2, p3)
    p = a + b + c
    k = 0.5 * p
    r = (sqrt(k * (k - a) * (k - b) * (k - c))) / k
    ox = (a * p3[0] + b * p2[0] + c * p1[0]) / p
    oy = (a * p3[1] + b * p2[1] + c * p1[1]) / p
    return (r, (int(ox), int(oy)))


def find_inscribed(markers, radius, img):
    for i in range(len(markers) - 2):
        for j in range(i + 1, len(markers) - 1):
            for k in range(j + 1, len(markers)):
                c1 = markers[i].center()
                c2 = markers[j].center()
                c3 = markers[k].center()
                r, incenter = inscribed_circle(c1, c2, c3)
                if r >= radius:
                    pts = np.array([c1, c2, c3], np.int32)
                    # pts = pts.reshape((-1,1,2))
                    cv2.polylines(img, [pts], True, (0, 255, 255))
                    cv2.circle(img, incenter, 50, (0, 255, 0), 3)
                    return

    # Camera intrinsics @1280x720


cmat = np.array([[6.4681449564160830e+03, 0., 6.3950000000000000e+02],
                 [0., 6.4681449564160830e+03, 3.5950000000000000e+02], [0., 0.,
                                                                        1.]])
cdist = np.array([[6.2915955520471334e+00], [-3.8557994750739471e+02], [0.0],
                  [0.0], [-1.2461115519292514e+00]])

# cap = cv2.VideoCapture(0)
# cap.set(3, 1280)
# cap.set(4, 720)

img = cv2.imread('raw.jpg')
undist = cv2.imread('thresh.jpg')
# ret, img = cap.read()
# cap.release()
# img = cv2.imread('images/dual-marker.jpg')
# undist = cv2.undistort(img, cmat, cdist)

t, p = vision.calibration.restore()
markers = vision.get_markers(transform=t, cam=False, frame=img)
centers = []


for m in markers:
    center = m.center()
    centers.append([[center[0], center[1]]])
    cv2.circle(undist, center, 10, (255, 120, 30), 3)
    # if m.mid == 1:
    #     trace.append(center)
    # if m.mid == 12:
    #     trace2.append(center)
    angle = m.orientation()
    cv2.circle(undist, (m.acx, m.acy), 5, (0, 0, 255), 2)
    ox = int(50 * cos(angle) + center[0])
    oy = int(50 * sin(angle) + center[1])
    cv2.line(undist, center, (ox, oy), (0, 255, 0), 4)
    # cv2.putText(frame, str(m.mid) + " angle: " + str(int(angle)-90), center, cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255))
    tx = center[0] + 40
    ty = center[1]
    cv2.putText(undist, str(m.mid), (tx, ty), cv2.FONT_HERSHEY_SIMPLEX, 1.5,
                (0, 0, 255), 4)
    cv2.drawContours(undist, [m.root], -1, (255, 0, 0), 1)
    if m.mid == 5:
        x = int(45.0 * cos(m.orientation()) + center[0])
        y = int(45.0 * sin(m.orientation()) + center[1])
        cv2.circle(undist, (x,y), 2, (255,0,0), 3)
    # cv2.circle(frame, center, 4, (255,0,0),1)

    # find_inscribed(markers, 50, undist)

    # print(markers[0].root)

    # cs = np.array(centers)
    # cv2.drawContours(undist, cs, -1, (255,0,0), 2)
    # print(cs)
    # hull = cv2.convexHull(cs)
    # print(hull)
    # for i in range(hull.shape[0]):
    #     start = tuple(hull[i][0])
    #     end = tuple(hull[(i+1)%hull.shape[0]][0])
    #     cv2.line(undist, start, end, (255,0,0), 3)
    # for i in range(cs.shape[0]):
    #     start = tuple(cs[i][0])
    #     end = tuple(cs[(i+1)%cs.shape[0]][0])
    #     cv2.line(undist, start, end, (255,0,0), 3)
    # rect = (hull[2,0,0], hull[2,0,1], hull[0,0,0], hull[0,0,1])
    # print(centers)
    # print(rect)
    # print(markers[0].root)
# vp, rp = vision.get_vacant_position(markers, 60)
# cv2.circle(undist, vp, 60, (255,0,0), 3)

# markers2 = [markers[0], markers[2], markers[3], markers[1]]
# # markers2.extend(markers[4:])
# corners = np.array([[[m.center()[0], m.center()[1]]] for m in markers2])
# rect = cv2.boundingRect(corners)
# print rect
# cv2.rectangle(undist, (rect[0], rect[1]), (rect[0]+rect[2], rect[1]+ rect[3]), (255,0,0,100), -1)
# subdiv = cv2.Subdiv2D(rect)
# # for c in centers:
# #     subdiv.insert(tuple(c[0]))
# for m in markers:
#     subdiv.insert(m.center())
d1 = euclidian_distance(markers[0].realxy()[:2], markers[2].realxy()[:2])
d2 = euclidian_distance(markers[0].center(), markers[2].center())
print(d1)
print(d2)

# # Check if a point is inside a rectangle
# def rect_contains(rect, point):
#     if point[0] <= rect[0]:
#         return False
#     elif point[1] <= rect[1]:
#         return False
#     elif point[0] >= rect[2]:
#         return False
#     elif point[1] >= rect[3]:
#         return False
#     return True


def draw_delaunay(cnt, img, subdiv, delaunay_color):
    triangleList = subdiv.getTriangleList()
    # print(triangleList)
    # size = img.shape
    # r = (0, 0, size[1], size[0])
    for t in triangleList:
        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])
        if cv2.pointPolygonTest(cnt, pt1, False) >= 0 and cv2.pointPolygonTest(
                cnt, pt2, False) >= 0 and cv2.pointPolygonTest(cnt, pt3,
                                                               False) >= 0:
            cv2.line(img, pt1, pt2, delaunay_color, 1, cv2.CV_AA, 0)
            cv2.line(img, pt2, pt3, delaunay_color, 1, cv2.CV_AA, 0)
            cv2.line(img, pt3, pt1, delaunay_color, 1, cv2.CV_AA, 0)


# cv2.drawContours(undist, corners, -1, (0, 0, 255), 3)
# draw_delaunay(corners, undist, subdiv, (255, 255, 255))

cv2.imwrite('overlay.jpg', undist)
cv2.imshow('live', undist)
# cv2.imshow('thresh', thresh)
# cv2.imshow('thresh2', th2)

key = cv2.waitKey(0)
cv2.destroyAllWindows()
