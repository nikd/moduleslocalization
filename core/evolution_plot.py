import sys
import pickle
import time
import csv

from pylab import *
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

def getData(file):
    with open(file, 'rb') as f:
        reader = csv.reader(f)
        data = list(reader)
        #print data

    print data[0][0]
    print data[0][1]
    print data[0][2]

    print ""
    print data[1][0]
    print data[1][1]
    print data[1][2]


    x=[]
    best = []
    mean = []
    popSize = 15
    generations = 20
    
    #Initial generation
    maxValue = 0
    average = 0
    for ind in range(popSize*2):
        value = float(data[ ind +1][2])
        average += value
        if value > maxValue:
            maxValue = value
    average /= (popSize*2.0)
    x.append(0)
    best.append(maxValue)
    mean.append(average)
    #Rest of the generations
    for g in range(generations-1):
        maxValue = 0
        average = 0
        for ind in range(popSize):
            value = float(data[2*popSize + g*popSize + ind +1][2])
            average += value
            if value > maxValue:
                maxValue = value
        average /= popSize
        x.append(g+1)
        best.append(maxValue)
        mean.append(average)

    #print x
    #print best
    
    return x, best, mean

def percentileColumn(matrix, i, p):
    col = [row[i] for row in matrix]
    return np.percentile(col, p)

def analyzeExperiment(folder, nExp, col):
    genAll = []
    bestAll = []
    meanAll = []

    
    for exp in range(nExp):
        fileName = folder + str(exp+1) +  "/exp_type3_plot.csv"
        gen, best, avg = getData(fileName)
        #plt.plot(gen, best,  color=col, linewidth=1, label='Champion in run 1')
        #plt.plot(gen, avg, color=col, linewidth=1, linestyle='--')
        
        
        
        genAll.append(gen)
        bestAll.append(best)
        meanAll.append(avg)

    #transform them into arrays
    genAll = np.asarray(genAll).astype(np.float)
    bestAll = np.asarray(bestAll).astype(np.float)
    meanAll = np.asarray(meanAll).astype(np.float)
    #genAll = np.asarray(genAll)
    #print genAll.ndim

    #calculate the mean
    genMean = np.median(genAll, axis=0)
    bestMedian = np.median(bestAll, axis=0)
    best_25 = np.percentile(bestAll, 25, axis=0)
    best_75 = np.percentile(bestAll, 75, axis=0)
    
    
    #meanMedian = np.median(meanAll, axis=0)
    #mean_25 = np.percentile(meanAll, 25, axis=0)
    #mean_75 = np.percentile(meanAll, 75, axis=0)
    
    return genMean, bestMedian, best_25, best_75, bestAll
    



def main():
    nExp = 6
    folder = "C:/Users/anfv/Documents/git/EmbodiedEvolution/core/output/finish_evolutions/new_tests/exp3/without_incremental_evolution/"
    col_normal= "red"
    gen, bestMedian_normal, best_25_normal, best_75_normal, bestAll_normal = analyzeExperiment(folder, nExp, col_normal)
    folder = "C:/Users/anfv/Documents/git/EmbodiedEvolution/core/output/finish_evolutions/new_tests/exp3/with_incremental_evolution/"

    col_incremental = "green"
    gen, bestMedian_incremental, best_25_incremental, best_75_incremental, bestAll_incremental = analyzeExperiment(folder, nExp, col_incremental)
    
    params = {
   'axes.labelsize': 12,
   'font.size': 12,
   'legend.fontsize': 12,
   'xtick.labelsize': 12,
   'ytick.labelsize': 12,
   'text.usetex': False,
   'figure.figsize': [5, 5],
   'figure.autolayout': True
   }
    rcParams.update(params)
    
    plt.grid(axis='y', color="0.9", linestyle='-', linewidth=1)
    
    evals=(gen+1)*15
    
    plt.plot(evals, bestMedian_normal,  color=col_normal, linewidth=3, label='non-incremental evolution')
    plt.fill_between(evals, best_25_normal, best_75_normal, alpha=0.25, linewidth=0, color=col_normal)
    plt.xlim([evals[0],evals[len(evals)-1]])
    
    plt.plot(evals, bestMedian_incremental,  color=col_incremental, linewidth=3, linestyle='--', label='incremental evolution')
    plt.fill_between(evals, best_25_incremental, best_75_incremental, alpha=0.25, linewidth=0, color=col_incremental)
    
    
    plt.legend()
    plt.legend = plt.legend(["Non-incremental evolution", "Incremental evolution"], loc=4);
    frame = plt.legend.get_frame()
    frame.set_facecolor('0.9')
    frame.set_edgecolor('0.9')
    frame.set_facecolor('1.0')
    frame.set_edgecolor('1.0')
    
    plt.axvline(x=((2.5+1)*15))
    
    # plt.title('EVO#1. Path from best genome in each generation')
    plt.ylabel('Fitness')
    plt.xlabel('Evaluation')
    #plt.axis([-0.5, 0.5, 0.75, 0.25])
    
    
    
    #analize the data
    g = 5
    x = []
    y = []
    for g in range(20):
        normal_i = bestAll_normal[:, g]
        incremental_i =  bestAll_incremental[:, g]
        statistic, pvalue = stats.ranksums(normal_i, incremental_i)
        statistic, pvalue = stats.mannwhitneyu(normal_i, incremental_i)
        #two tailed test
        pvalue *= 2
        if(pvalue < 0.05):
            print "significant difference in generation " + str(g) +", pvalue: " + str(pvalue)
            x.append((g+1)*15)
            y.append(0.57)
    plt.plot(x, y, linestyle='None', marker="D", color="black", markersize=6)
    plt.show()
#    print "End"
#    print normal_i
#    print incremental_i

if __name__ == '__main__':
    args = sys.argv[1:]
    main(*args)


