import vision
import ur5
import random
from math import pi

def pnp_single_tower_random_test(runs=10, source=0):
    r = ur5.load()
    r.home()
    m = vision.get_markers(source=source)
    if len(m) < 5:
        print("Not enough markers")
        return
    t = vision.calibration.redo_transform(m)
    m = vision.get_markers(t, source=source)
    if len(m) < 5:
        print("Not enough markers")
        return
    for i in range(0, runs):
        x = random.uniform(0.3, 0.65)
        y = random.uniform(-0.4, 0.4)
        o = random.uniform(0.0, 2*pi)
        r.pick_and_place(m[4], (x, y), o)
        r.home()
        m = vision.get_markers(t, source=source)
        if len(m) < 5:
            print("Marker no longer detected")
            break
    r.cleanup()

def pnp_single_tower_matrix_test():
    r = ur5.load()
    f = open('matrix_error.csv', 'w')
    f.write('real_x,real_y,vision_x,vision_y\n')
    r.home()
    m = vision.get_markers()
    t = vision.calibration.redo_transform(m)
    m = vision.get_markers(t)
    x = 0.3
    while x <= 0.65:
        y = -0.4
        while y <= 0.4:
            r.pick_and_place(m[4], (x, y))
            r.home()
            m = vision.get_markers(t)
            vxy = m[4].realxy()
            ang = m[4].orientation()
            f.write(str(x)+','+str(y)+','+str(vxy[0])+','+str(vxy[1])+','+str(ang)+'\n')
            y += 0.2
        x += 0.1166
    r.cleanup()
    f.close()
