import urx
import logging
import math3d
import sys
import time
from vision.TrackerFai import get_marker_object, get_vacant_position, euclidian_distance, get_markers, get_marker_object_fast
from math import pi, sqrt, fabs, pow, degrees
import random
import numpy as np
from collections import deque


def load():
    instance = Mother()
    instance.initialize()
    return instance


class Mother:
    def __init__(self):
        logging.basicConfig(level=logging.WARN)
        self.rob = None
        self.vel = 1.5 #1.4
        self.acc = 1.0 #0.4
        self.vel_home = 1.5 #1.5 
        self.acc_home = 1.0 #0.5
        self.acc_pick = 0.5 #0.07
        self.vel_pick = 1.5 #0.03
        self.travel_z = -0.04
        self.pick_distance = -0.13064107
        self.pick_force = 60
        self.pick_offset = 0.0019   #0.0035
        self.place_distance = self.pick_distance + 0.003
        self.safe_place_radius = 0.07
        self.at_home = False
        self.exceptionCount = 0

    def initialize(self):
        if self.rob == None:
            # TODO: Avoid hardcoding robot init values- read from config
            retry_count = 10
            while retry_count > 0 and self.rob == None:
                try:
                    self.rob = urx.Robot("169.254.116.100", use_rt=True)
                except:
                    retry_count = retry_count - 1
            self.rob.set_tcp((0, 0, 0.034, 0, 0, 0))
            self.rob.set_payload(0.1, (0, 0, 0))
        else:
            self.rob.close()
            self.rob = None
            self.initialize()

    def home(self):
        """Move the tool to the home position where it does not interfere
        with the camera"""
        if self.rob != None and self.rob.is_running():
#            homeOld= (pi / 2, -pi / 2, pi / 2, -pi / 2, -pi / 2, 0.0)
#            homeInt= (pi, -75*pi/180, pi / 2, -pi / 2, -pi / 2, 0.0)
            home = (pi, -78.57*pi/180, 160.5*pi/180, -172.1*pi/180, -pi/2, -pi/2)
#            self.rob.movej(
#                homeOld,
#                acc=self.acc_home/4,
#                vel=self.vel_home/4)
#            self.rob.movej(
#                homeInt,
#                acc=self.acc_home/4,
#                vel=self.vel_home/4)
            self.rob.movej(
                home,
                acc=self.acc_home,
                vel=self.vel_home)
            self.home_orientation = self.rob.get_orientation()
            self.at_home = True

    def build(self, positions, relax_dist=0.0, relax_angle=0.0):
        """Move markers to positions determined by the given task.
        This must be done safely ie. without moving into other objects
        """
        moves = []
        conflicts = {}
        late_moves = {}
        markers = get_markers()
        # check for position conflicts
        for mid, to in positions.iteritems():
            conflict_markers = []
            for m in markers:
                if m.isMoving == True:
                    #print "1, Using fast tracker"
                    m = get_marker_object_fast(m.mid, self)
                rxy = m.realxy()
                # euclidian distance between object center-points
                # dist = sqrt(pow(fabs(to[0][0]-rxy[0]),2)+pow(fabs(to[0][1]-rxy[1]),2))
                dist = euclidian_distance(to[0], rxy[:2])
                if dist < self.safe_place_radius and mid != m.mid:
                    conflict_markers.append(m.mid)
            if len(conflict_markers) > 0:
                conflicts[mid] = conflict_markers
        # if there are conflicts moves need to be ordered
        if len(conflicts) > 0:
            # safely move markers with no conflicts (highest priority)
            for k in positions.keys():
                if k not in conflicts:
                    moves.append(k)
            # resolve conflicts
            resolving = True
            while len(conflicts) > 0 and resolving:
                # resolving will stay false if no moves could be made
                resolving = False
                for mid, cs in conflicts.items():
                    new_conflict = []
                    for c in cs:
                        if c not in conflicts:
                            # conflicting marker not in conflict = safe move
                            resolving = True
                        else:
                            # a conflict still needs to be resolved
                            new_conflict.append(c)
                    if len(new_conflict) == 0:
                        # delete the entry if no more conflicting markers
                        moves.append(mid)
                        del conflicts[mid]
                    else:
                        # update the conflicting markers
                        conflicts[mid] = new_conflict
                # TODO: handle if all conflicts have not been resolved
                # potential circular conflict: {3:[4], 4:[3]}
                if not resolving and len(conflicts) > 0:
                    if len(moves) > 0:
                        vpos, ppos = get_vacant_position(markers, 85, moves, positions)
                    else:
                        vpos, ppos = get_vacant_position(markers, 85)
                    t_mid = conflicts.keys()[0]
                    moves.append(t_mid)
                    del conflicts[t_mid]
                    late_moves[t_mid] = positions[t_mid]
                    positions[t_mid] = ((ppos[0], ppos[1]), 0)
                    resolving = True
        else:
            moves = positions.keys()
        # perform pick-and-place ops
        for m in moves:
            if m in positions:
                to, angle = positions[m]
                marker = get_marker_object(m, self)
                if marker.isMoving == True:
                    #print "2, Using fast tracker"
                    marker = get_marker_object_fast(m, self)
                self.pick_and_place(marker, to, angle, relax_dist, relax_angle)
                # update marker if late move required
                if marker.mid in late_moves:
                    marker.realxy()
                    marker.rxy[0] = to[0]
                    marker.rxy[1] = to[1]
                    marker.angle = 0.0
        # perform postponed - "late" - pnp ops
        for mid, pos in late_moves.iteritems():
            to, angle = pos
            marker = get_marker_object(mid, self)
            if marker.isMoving == True:
                #print "3, Using fast tracker"
                marker = get_marker_object_fast(mid, self)
            self.pick_and_place(marker, to, angle, relax_dist, relax_angle)
        self.home()
        return (moves, late_moves)

    def pick_and_place(self, marker, to, to_angle=0, relax_dist=0.0, relax_angle=0.0):
        ''' Pick and place object defined by marker

        Keyword arguments:
        marker -- marker object defining the object to be manipulated
        to     -- position to move place to
        '''
        dist = euclidian_distance(marker.realxy()[:2], to)
        angle_diff = fabs(marker.orientation() - to_angle)
        if dist > relax_dist or angle_diff > relax_angle:
            self._goto_coordinate(marker.realxy()[:2], marker.orientation())
            self._pick_up_object(height=marker.realxy()[2], distance = False)
            self._goto_coordinate(to, to_angle)
            self._place_object(height=marker.realxy()[2])
        # TODO: update marker location??

    def _goto_coordinate(self, to, angle=0.0):
        ''' Translation in x,y plane from current position to *to*
        at travel-z height

        Keyword arguments:
        to    -- list containing at least x and y destination coordinates
        angle -- the angle (z-rotation) of the destination position
        '''
        joints = self.rob.getj()
        originalAngle = angle
        if fabs(joints[5] + angle )>= 2*pi:
            # reduce or increse the angle to be within the limits
            if(joints[5] + angle) >= 2*pi:
                angle -=2*pi
            else:
                angle += 2*pi
        print "angle= " + str(degrees(angle)) + ", originalAngle: " + str(degrees(originalAngle)) + ", joint: " + str(degrees(joints[5]))
        trans = self.rob.get_pose()
        if len(to) < 3:
            trans.pos = math3d.Vector((to[0], to[1], self.travel_z))
        else:
            trans.pos = math3d.Vector(to)
        o = self.home_orientation.copy()
        o.rotate_zt(angle)
        trans.orient = o
        # movel seems most appropriate since it forces a linear path from
        # 'home' pose to the new pose
        # TODO: movement may be improved using waypoints
        self._safe_set_pose(trans, 'movel', self.acc, self.vel)
        self.at_home = False

    def goto_marker(self, marker):
        self._goto_coordinate(marker.realxy(), marker.orientation())

    def _pick_up_object(self, distance=True, height=None):
        ''' Pick up object at *pic_distance* from current tool position
        or pick up using force-sensing. TCP must be directly above
        the object.

        Keyword arguments:
        distance -- use distance if true otherwise use force
        height   -- the z-coordinate to move to for pick-up
        '''
        # move to object
        pose = self.rob.getl()
        if distance:
            if height is not None:
                pose[2] = height - self.pick_offset + pose[1]/0.40*0.005 + (pose[0]-0.65)/0.4*0.005
                print (self.pick_offset - pose[1]/0.40*0.005 - (pose[0]-0.7)/0.45*0.005)
            else:
                pose[2] = self.pick_distance - 0.005
            self._safeMovel(pose, acc=self.acc_pick, vel=self.vel_pick)
        else:
            forceVector = deque([])
            pose[2] = height - self.pick_offset
            self._safeMovel(pose, acc=self.acc_pick, vel=self.vel_pick)
            offset = 0.015
            pose[2] = self.pick_distance - offset
            self.rob.movel(pose, acc=0.05, vel=0.008, wait=False)
            force = 30.0
            while force < self.pick_force:  # TODO: get this force right - there seems to be a lot of noise
                forceTmp = self.rob.get_force()
                force = force * 4/5 + forceTmp*1/5
                forceVector.append(forceTmp)
                if (len(forceVector) > 9):
                    forceVector.popleft()
                #print "Force: " + str(force) + " - " + str(forceTmp)  + " - " + str(np.average(forceVector))
                force = np.average(forceVector) 
                time.sleep(0.01)
                pose = self.rob.getl()
                if pose[2] < self.pick_distance - offset:
                    break
#                if not self.rob.is_program_running():
#                    break
            #print("force after loop: ", force)
            self.rob.stopl()
        #self.rob.set_payload(0.5, (0,0,0))
        #time.sleep(1)
        # retract
        pose = self.rob.getl()
        pose[2] = self.travel_z
        self._safeMovel(pose, acc=self.acc_pick, vel=self.vel_pick)

    def _safeTranslate(self, pose, a, v):
        while True:
            try:
                self.rob.translate(pose, a, v)
                break
            except:
                print "Probem with the UR5 arm"
                time.sleep(4)
            
    def _place_object(self, height=None):
        ''' Place object at *place_distance* in -z from current TCP.
        When place the magnet will be energized to release the payload.

        Keyword arguments:
        height   -- the z-coordinate to move to for placement
        '''
        # move to place height
        pose = self.rob.getl()
        if height is not None:
            pose[2] = height + self.pick_offset/2
        else:
            pose[2] = self.place_distance
        self._safeMovel(pose, acc=self.acc_pick, vel=self.vel_pick)
        # energize magnet (release payload)
        self.rob.set_digital_out(
            8, True)  # TODO: fix digital output to comply with new API
        #self.rob.set_payload(0.1, (0,0,0))
        time.sleep(0.1)
        # retract
        pose = self.rob.getl()
        pose[2] = self.travel_z
        self._safeMovel(pose, acc=self.acc_pick, vel=self.vel_pick)
        # de-energize magnet
        self.rob.set_digital_out(8, False)

    def cleanup(self):
        self.rob.set_digital_out(8, False)
        self.rob.close()
        
    def _safeMovel(self, pose, acc, vel):
        firstTime = True
        while True:
            try:
                self.rob.movel(pose, acc=acc, vel=vel)
                break
            except:
                if firstTime is True:
                    self.exceptionCount += 1
                    firstTime = False
                print "Probem with the UR5 arm"
                time.sleep(4)
    def _safeMovej(self, pose, acc, vel):
        firstTime = True
        while True:
            try:
                self.rob.movej(pose, acc=acc, vel=vel)
                break
            except:
                if firstTime is True:
                    self.exceptionCount += 1
                    firstTime = False
                print "Probem with the UR5 arm"
                time.sleep(4)
                
    
    def _safe_set_pose(self, trans, cmd, a, v):
        firstTime = True
        while True:
            try:
                self.rob.set_pose(trans, command=cmd, acc=a, vel=v)
                break
            except:
                if firstTime is True:
                    self.exceptionCount += 1
                    firstTime = False
                print "Probem with the UR5 arm"
                time.sleep(4)
            
    def getExceptionCount(self):
        tmp = self.exceptionCount
        self.exceptionCount = 0
        return tmp
        
