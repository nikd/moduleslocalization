import vision
import urx
import logging
import sys
import random
import numpy as np

class Fake:

    def getl(self):
        p = []
        for val in range(6):
            p.append(random.random() * 0.8)
        return p

    def close(self):
        print("Connection closed")

def auto_calibrate(rob):
    cid = 12
    destinations = []
    # TODO: implement rest of auto-calibrate


def calibrate(rob):
    print("--- Calibration initiated ---")
    print("1. Obtain real-world TCP points")
    poses = []
    raw_input("Move TCP to marker 1 - then press ENTER.")
    poses.append(rob.getl())
    print("INFO: Pose 1 obtained:")
    print(poses[0])
    raw_input("Move TCP to marker 2 - then press ENTER.")
    poses.append(rob.getl())
    print("INFO: Pose 2 obtained:")
    print(poses[1])
    raw_input("Move TCP to marker 3 - then press ENTER.")
    poses.append(rob.getl())
    print("INFO: Pose 3 obtained:")
    print(poses[2])
    raw_input("Move TCP to marker 4 - then press ENTER.")
    poses.append(rob.getl())
    print("INFO: Pose 4 obtained:")
    print(poses[3])
    print("INFO: Closing robot connection.")
    rob.close()

    print("2. Detecting fixed markers...")
    raw_input("Move robot out of image - then press ENTER.")
    markers = vision.get_markers(None, True, None, True, 1, False)
    t = np.zeros((3,3))
    if len(markers) < 4:
        print(len(markers))
        print("ERR: Too few markers in image. Skipping transform.")
        md = {}
        for m in markers:
            md[m.mid] = m
        if not md.has_key(1):
            print("ERR: Marker 1 missing.")
        if not md.has_key(2):
            print("ERR: Marker 2 missing.")
        if not md.has_key(3):
            print("ERR: Marker 3 missing.")
        if not md.has_key(4):
            print("ERR: Marker 4 missing.")
    else:
        md = {}
        for m in markers:
            md[m.mid] = m
        if not md.has_key(1):
            print("ERR: Marker 1 missing.")
        elif not md.has_key(2):
            print("ERR: Marker 2 missing.")
        elif not md.has_key(3):
            print("ERR: Marker 3 missing.")
        elif not md.has_key(4):
            print("ERR: Marker 4 missing.")
        else:
            print("INFO: Found markers 1, 2, 3 and 4.")
            print("3. Computing transformation matrix")
            try:
                t = vision.calibration.get_transform(markers, poses)
            except ValueError as e:
                print("ERR: Could not solve for transformation matrix:")
                print(e.message)
            print("INFO: Obtained transformation matrix:")
            print(t)

    print("4. Saving calibration data to cdata.json")
    vision.calibration.save(t, poses)
    print("--- Calibration ended ---")

if __name__ == "__main__":
    logging.basicConfig(level=logging.WARN)
    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        robot = Fake()
    else:
        robot = urx.Robot("169.254.116.100")
    calibrate(robot)

