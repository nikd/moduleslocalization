import sys
import pickle
import time
import matplotlib.pyplot as plt
from pylab import *

def interval_map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def main(folder="C:/Users/anfv/Documents/git/EmbodiedEvolution/core/output/finish_evolutions/new_tests/exp3/with_incremental_evolution/5/", nofl='20', prefix='best_positions_'):
    nofiles = int(nofl)
    files = [(folder+prefix+str(n),n) for n in range(nofiles)]
    #print files
    print('Reading '+str(len(files))+ 'files...')




    for f,g in files:
        with open(f, 'r') as fh:


            print "generation : ", g
            #print f
            ps = pickle.load(fh)
            
            sensorLfile =  folder+"best_leftsensor_"+str(g)
            with open(sensorLfile, 'r') as slh:
                sl = pickle.load(slh)
            
            sensorRfile =  folder+"best_rightsensor_"+str(g)
            with open(sensorRfile, 'r') as srh:
                sr = pickle.load(srh)
                
            actionFile = "folder"+"best_actions_"+str(g)
            
            #print "sensor L: ",sl
            #print "sensor R: ",sr
            #print ps

            ps1 = [ps[:1]]
            ps2 = [ps[1:]]

            xs1 = [x for x,y,z in ps1[0][0]]
            ys1 = [y for x,y,z in ps1[0][0]]
            xs2 = [x for x, y, z in ps2[0][0]]
            ys2 = [y for x, y, z in ps2[0][0]]
            gs1 = interval_map(g+1, 1, nofiles, 0.95, .3)
            print "gs1: ", gs1
            if g < 3:
                
                plt.figure(1)
                startPos = ((xs1[0]-0.5)**2 + (ys1[0]-0.35)**2)**0.5
                #if startPos <.1:
                plt.plot(ys1,xs1,color=str(gs1), linewidth=1.5)

                plt.figure(2)
                startPos = ((xs2[0] - 0.5) ** 2 + (ys2[0] - 0.35) ** 2) ** 0.5
                #if startPos < 0.1:
                plt.plot(ys2, xs2, color=str(gs1), linewidth=1.5)
                #plt.show()
            else:
                plt.figure(3)
                startPos = ((xs1[0]-0.5)**2 + (ys1[0]-0.35)**2)**0.5
                #if startPos <.1:
                plt.plot(ys1,xs1,color=str(gs1), linewidth=1.5)

                plt.figure(4)
                startPos = ((xs2[0] - 0.5) ** 2 + (ys2[0] - 0.35) ** 2) ** 0.5
                #if startPos < 0.1:
                plt.plot(ys2, xs2, color=str(gs1), linewidth=1.5)



    params = {
   'axes.labelsize': 16,
   'font.size': 16,
   'legend.fontsize': 16,
   'xtick.labelsize': 16,
   'ytick.labelsize': 16,
   'text.usetex': False,
   'figure.figsize': [3, 3],
   'figure.autolayout': True
   }
    rcParams.update(params)

    #Add start positions and axis labels
    for i in range(4):
        plt.figure(i+1)
        plt.text(0.38, 0.507, 'Start', fontsize=16)
        plt.plot(0.35, 0.50, 'rx', markersize=16)
        plt.text(-0.482, 0.507, 'Target', fontsize=16)
        plt.plot(-0.35, 0.50, 'b^', markersize=16)
        # plt.title('EVO#1. Path from best genome in each generation')
        plt.xlabel('Real-world Y-axis')
        plt.ylabel('Real-world X-axis')
        plt.axis([-0.5, 0.5, 0.75, 0.25])
      
    markerSize = 24    
    plt.figure(1)
    plt.plot(0.16, 0.67, 'ro', markersize=markerSize)
    plt.plot(0.08, 0.60, 'ro', markersize=markerSize)
    plt.plot(0.00, 0.53, 'ro', markersize=markerSize)
    plt.plot(-0.10, 0.6, 'ro', markersize=markerSize)

    plt.figure(2)
    plt.plot(0.16, 0.33, 'ro', markersize=markerSize)
    plt.plot(0.08, 0.40, 'ro', markersize=markerSize)
    plt.plot(0.00, 0.47, 'ro', markersize=markerSize)
    plt.plot(-0.1, 0.40, 'ro', markersize=markerSize)
    
    plt.figure(3)
    plt.plot(0.20, 0.67, 'ro', markersize=markerSize)
    plt.plot(0.00, 0.47, 'ro', markersize=markerSize)
    plt.plot(-0.17, 0.7, 'ro', markersize=markerSize)
    plt.plot(0.10, 0.37, 'ro', markersize=markerSize)

    plt.figure(4)
    plt.plot(0.20, 0.33, 'ro', markersize=markerSize)
    plt.plot(0.00, 0.53, 'ro', markersize=markerSize)
    plt.plot(-0.17, 0.3, 'ro', markersize=markerSize)
    plt.plot(0.10, 0.63, 'ro', markersize=markerSize)

    plt.show()


if __name__ == '__main__':
    args = sys.argv[1:]
    main(*args)
