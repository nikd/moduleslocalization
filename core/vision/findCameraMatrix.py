#! /usr/bin/python

import time
import numpy as np
import cv2
import glob


__author__ = "anfv"
__date__ = "$18-Nov-2016 16:18:51$"

if __name__ == "__main__":
    print "Hello World";

takePhotos = True
if(takePhotos==True):
    cap = cv2.VideoCapture(0)
    cap.set(3, 1920)#1920
    cap.set(4, 1080)#1080
    font = cv2.FONT_HERSHEY_SIMPLEX
    
    for j in range(30):
        ret, frame = cap.read()
        cv2.imshow('capture', frame)
        cv2.waitKey(100)
        
    for i in range(20):
        for j in range(30):
            ret, frame = cap.read()
            cv2.putText(frame, str(i), (10, 25), font, 1, (255, 255, 255), 2, cv2.LINE_AA) 
            cv2.imshow('capture', frame)
            cv2.waitKey(100)
        ret, frame = cap.read()
        cv2.imwrite('test_data/cal_img/'+str(i)+'.jpg', frame)
        

#for file in os.listdir('test_data/cal_img/'):
#    if file.endswith(".jpg"):
#        #print(file)
#        name = 'test_data/cal_img/' + str( _counter%photos) + '.jpg'


# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('test_data/cal_img/*.jpg')

for fname in images:
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)

        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv2.drawChessboardCorners(img, (9,6), corners2,ret)
        
        print np.asarray(objpoints).shape
        print np.asarray(imgpoints).shape
        print fname

    cv2.imshow('img',img)
    cv2.waitKey(50)


print np.asarray(objpoints).shape
print np.asarray(imgpoints).shape

objpoints = np.array(objpoints,dtype=np.float32) 
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
cv2.destroyAllWindows()


print mtx

print dist