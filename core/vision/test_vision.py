import unittest
import calibration
import marker
import json
import numpy as np
from math import pi

class TestCalibration(unittest.TestCase):

    def setUp(self):
        self.datapath = 'test_data/test_cdata.json'
        # with open(self.datapath, 'r') as f:
        #     data = json.load(f)
        # self.poses = data['poses']
        pass

    def tearDown(self):
        pass

    def test_save_and_restore(self):
        transform = np.array([1.0,2.0,3.0,4.0,5.0])
        poses = [1.1, 4.4, 5.5, .6, .8]
        calibration.save(transform, poses, self.datapath)
        t,p = calibration.restore(self.datapath)
        self.assertEqual(t.all(), transform.all(), 'transform integrity check failed')
        self.assertEqual(p, poses, 'poses integrity check failed')


class TestMarker(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_angle_trunc(self):
        c1 = marker.Marker.angle_trunc(1.0)
        c2 = marker.Marker.angle_trunc(-1.0)
        c3 = marker.Marker.angle_trunc(0)
        c4 = marker.Marker.angle_trunc(-7.0)
        self.assertAlmostEqual(c1, 1.0, 'positive angle error')
        self.assertAlmostEqual(c2, -1.0 + (pi*2), 'negative, no-loop, angle error')
        self.assertAlmostEqual(c3, 0, 'zero angle error')
        self.assertAlmostEqual(c4, -7.0 + (pi*4), 'negative, loop, angle error')

    def test_angle_between_points(self):
        c1 = marker.Marker.angle_between_points(0, 0, 10, 10)
        self.assertAlmostEqual(c1, pi/4, 'angle calculation error')
