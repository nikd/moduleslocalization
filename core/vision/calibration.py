import numpy as np
import json
import cv2
import os.path

def solve_transform(a, b):
    if len(a) >= 3 and len(b) == len(a):
        t = np.linalg.solve(a, b)
        return t
    else:
        raise ValueError("Point matrices length mismatch")

def get_transform(markers, poses):
    """Process marker and pose arrays containing matching points
    and return the resulting transformation matrix

    Keyword arguments:
    markers -- marker array with at least identified markers 1,2 and 3 (reference points)
    poses   -- poses (x, y, z, rx, ry, rz) from TCP touching said markers in real world
    """
    return get_transform_lstsqr(markers, poses)

def get_transform_std(markers, poses):
    markers.sort(key=lambda m: m.mid)
    a = np.array([[m.center()[0], m.center()[1], 1.0] for m in markers if m.mid <= 3])
    b = np.array([p[:2] + [1.0] for p in poses])
    return solve_transform(a, b)

def get_transform_lstsqr(markers, poses):
    markers.sort(key=lambda m: m.mid)
    #b = np.array([p[:3] for p in poses])
    #t, res, rank, s = np.linalg.lstsq(a, b)
    a = np.array([[m.center()[0], m.center()[1], 1.0] for m in markers if (m.mid <= 4 and m.mid > 0)])
    b = np.array([(p[:2] + [1.0]) for p in poses])
    t, retval = cv2.findHomography(a, b)
    t/=t[2,2]
    return t

def restore(cfile='cdata.json'):
    if(os.path.isfile(cfile)): 
        with open(cfile, 'r') as f:
            data = json.load(f)
    else:
        cfile = "../" + cfile
        with open(cfile, 'r') as f:
            data = json.load(f)
    return (np.array(data['transform']), data['poses'], data['height'])

def save(transform, poses, height, cfile='cdata.json'):
    if(os.path.isfile(cfile)): 
        with open(cfile, 'w') as f:
            obj = {'transform': transform.tolist(), 'poses': poses, 'height': height}
            json.dump(obj, f)
    else:
        cfile = "../" + cfile
        with open(cfile, 'w') as f:
            obj = {'transform': transform.tolist(), 'poses': poses, 'height': height}
            json.dump(obj, f)

def redo_transform(markers):
    old_transform, poses, _= restore()
    transform = get_transform(markers, poses)
    height = np.average(poses, 0)[2]
    save(transform, poses, height)
    return transform, height

