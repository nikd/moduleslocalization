#
# Marker module
import cv2
import numpy as np
from math import pi, cos, sin, atan2, degrees, fabs, sqrt, radians
import calibration as cal
import inspect


class Marker(object):

    def __init__(self, mid, root, l1, l2, transform=None, height = -10):
        self.mid = mid
        self.root = root
        self.black_leaves = l1
        self.white_leaves = l2
        self.transform = transform
        self.height = height
        if(l1 is not None or l2 is not None):
            self.no_bl = len(l1)
            self.no_al = len(l1) + len(l2)
        self.cx, self.cy = None, None
        self.acx, self.acy = None, None
        self.rxy = None
        self.angle = None
        self.isMoving = False

    def center(self):
        if self.cx is None or self.cy is None:
            M = cv2.moments(self.root)
            self.cx = int(M['m10'] / M['m00'])
            self.cy = int(M['m01'] / M['m00'])
#            print "cx: ", self.cx, " cy: ", self.cy
            # robot offset hack
#            if self.mid == 5:
#                corrected_angle = self.orientation()
#                self.cx = 51.0 * cos(corrected_angle) + self.cx
#                self.cy = 51.0 * sin(corrected_angle) + self.cy
        return (self.cx, self.cy)

    def orientation(self):
        if self.angle == None:
            sum_all, sum_black = (0, 0), (0, 0)
            for l in self.black_leaves:
                ((cx, cy), size, angle) = cv2.minAreaRect(l)
                sum_all = (sum_all[0] + cx, sum_all[1] + cy)
                sum_black = (sum_black[0] + cx, sum_black[1] + cy)
            for l in self.white_leaves:
                ((cx, cy), size, angle) = cv2.minAreaRect(l)
                sum_all = (sum_all[0] + cx, sum_all[1] + cy)
            avg_all = (int(sum_all[0] / self.no_al), int(sum_all[1] /
                                                         self.no_al))
            avg_black = (int(sum_black[0] / self.no_bl), int(sum_black[1] /
                                                             self.no_bl))
            self.angle = Marker.angle_between_points(
                avg_all[0], avg_all[1], avg_black[0], avg_black[1])
            # Save alternative averaged centroid
            self.acx = avg_all[0]
            self.acy = avg_all[1]
            self.acx_black = avg_black[0]
            self.acy_black = avg_black[1]
        return self.angle

    def realxy(self):
        if self.rxy is None:
            if self.transform is not None:
                (cxs, cys) = self.center()
                #self.rxy = np.dot([cxs, cys, 1.0], self.transform)
                self.rxy = np.dot(self.transform, [cxs, cys, 1.0])
                self.rxy /= self.rxy[2]
                self.rxy[2] = self.height
#                if self.mid == 5:
#                    print "orientation: ", self.orientation()*180/3.1415
#                    print "before adding the offset: ", self.rxy[0], ", ", self.rxy[1]
#                    self.rxy[0] = 0.040 * sin(self.orientation()) + self.rxy[0]
#                    self.rxy[1] = 0.040 * cos(self.orientation()) + self.rxy[1]
#                    print "after adding the offset: ", self.rxy[0], ", ", self.rxy[1]
        return self.rxy
    def updateMarker(self, cx, cy, angle):
        self.cx = cx
        self.cy = cy
        self.angle = angle
    
    def getAlternativeAngle(self):
        return self.acx, self.acy, self.acx_black, self.acy_black

    @staticmethod
    def getMarkerModelPoints(scalar=1):
        return scalar * np.array([
            (0.0, 0.0, 0.0),        # Middle point
            (0.0, 14.44, 0.0),      # Top Lower-Center
            (4.41, 19.03, 0.0),     # Top Upper-Right
            (-4.41, 19.03, 0.0)])   # Top Upper-Left

    @staticmethod
    def getMarkerModelPointsAlt(scalar=1):
        return scalar * np.array([
            (-13.95, 0.0, 23.0),  # upper-left
            (13.95, 0.0, 23.0),  # upper-right
            (23, 0.0, 13.95),  # right-upper
            (23, 0.0, -13.95),  # right-lower
            (13.95, 0.0, -23.0),  # lower-right
            (-13.95, 0.0, -23.0),  # lower-left
            (-23, 0.0, -13.95),  # left-lower
            (-23, 0.0, 13.95)])  # left.upper
        return scalar * np.array([
            (-13.95, 0.0, 23.0),  # upper-left
            (23, 0.0, 13.95),  # right-upper
            (13.95, 0.0, -23.0),  # lower-right
            (-23, 0.0, -13.95), ])  # left-lower


        
    @staticmethod
    def angle_trunc(a):
        while a < 0.0:
            a += pi * 2
        return a

    @staticmethod
    def angle_between_points(x_start, y_start, x_end, y_end):
        deltaY = y_end - y_start
        deltaX = x_end - x_start
        return Marker.angle_trunc(atan2(deltaY, deltaX))
