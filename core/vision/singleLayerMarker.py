import cv2
import numpy as np
from marker import Marker
from math import pi, cos, sin, atan2, degrees, fabs, sqrt, radians

class SingleLayerMarker(Marker):
    def __init__(self, mid, root, l1, l2, tp, eular, transform=None, height=-10):
        self.tp = tp
        self.eular = eular
        super(SingleLayerMarker, self).__init__(mid, root, l1, l2, transform, height)

    def orientation(self):
        if self.angle == None:
            (cx, cy) = self.center()
            (tx, ty) = self.tp
            self.angle = Marker.angle_between_points(cx, cy, tx, ty)
        return self.angle

    # Used in safe markers
    def getAlternativeAngle(self):
        return 0, 1, 0, 1
