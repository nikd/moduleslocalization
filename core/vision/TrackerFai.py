#
# TrackerFai module using multi threading
import threading
from copy import deepcopy
from marker import Marker
from singleLayerMarker import SingleLayerMarker
from collections import Iterable
import calibration
import cv2
import numpy as np
from math import pi, cos, sin, atan2, degrees, fabs, sqrt, radians
import calibration as cal
import inspect
import time
import os
from collections import deque
import math 


lock = threading.Lock()
safeMarkers = []
currentMarkers = []
performCalibration = False

# Default marker trees
def_markers = {
    '0122222212212121111': 1,
    '0122212221222121111': 2,
    '0122212221221221111': 3,
    '0122212221212121111': 4,
    '0122222212221211111': 5,
    '0122221221221221111': 6,
    '0122212212212211111': 7,
    '0122221222122121111': 8,
    '0122222222212111111': 9,
    '0122221221212121111': 10,
    '0122222122221211111': 11,
    '0122221222122211111': 12
}

# Small marker trees
small_markers = {
    '0122222211111': 1,
    '0122222221111': 2,
    '0122222121111': 3,
    '0122212211111': 4,
    '0122212121111': 5,
    '0122221211111': 6,
    '0122122121111': 7,
    '0122122111111': 8,
    '0122121211111': 9,
    '0122221221111': 10,
    '0121212121111': 11,
    '0122212221111': 12,

}

varying__single_layer_markers = {
    '0111':      {
        'b':        1,
        'l':        2,
        'r':        3
    },
    '01111':     {
        'bb':       4,
        'lb':       5,
        'br':       6,
        'll':       7,
        'lr':       8,
        'rr':       9,
    },
    '011111':    {
        'lbb':      10,
        'bbr':      11,
        'llb':      12,
        'lbr':      13,
        'brr':      14,
        'llr':      15,
        'lrr':      16,
    },
    '0111111':   {
        'llbb':     17,
        'lbbr':     18,
        'bbrr':     19,
        'llbr':     20,
        'lbrr':     21,
        'llrr':     22,
    },
    '01111111':  {
        'llbbr':    23,
        'lbbrr':    24,
        'llbrr':    25,
    },
    '011111111': {
        'llbbrr':   26
    }
}

# Camera intrinsics @1280x720

#RMS=0.35
cmat = np.array(
[[ 1.35158009e+03,   0.00000000e+00,   7.64722141e+02],
[  0.00000000e+00,   1.34775356e+03,   4.46430604e+02],
[  0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
cdist = np.array( [ [-3.07426183e-02],   [1.57294154e-01],  [-1.76543289e-03],   [4.46479392e-04],  [-7.72334003e-01]])

invalid_marker = (False, None)

#RMS=0.95
#cmat = np.array([[  1.33996644e+03,   0.00000000e+00,   7.75274684e+02],
#[  0.00000000e+00,   1.33754402e+03,   4.32694633e+02],
#[  0.00000000e+00,   0.00000000e+00,   1.00000000e+00]])
#cdist = np.array([  2.21364604e-01,  -2.35994747e+00,  -1.76918573e-03,  -3.67498637e-05,  6.98277835e+00])

# Colors
red = (0,0,255)
yellow = (0, 255, 255)
green = (0, 255, 0)
blue = (255, 0, 0)
cyan = (255, 255, 0)
purple = (255, 0, 255)
white = (255, 255, 255)

def calibrate():
    global performCalibration
    performCalibration = True
    while(performCalibration == True):
        time.sleep(0.1)
    print "Calibration finished!"
    time.sleep(1)


def get_markers():
    lock.acquire()
    markers = safeMarkers
    lock.release()
    return markers

def get_marker_object(mid, ur5 = None):
    idx = -1
    marker = None
    markers = get_markers()
    while marker is None:
        for i, m in enumerate(markers):
            if mid == m.mid:
                return m
        if(ur5 is not None):
            if(ur5.at_home == False):
                ur5.home()
        time.sleep(0.1)
        print "Marker " + str(mid) + " not found. Waiting..."
        markers = get_markers()
        
def get_marker_object_fast(mid, ur5 = None):
    global currentMarkers
    idx = -1
    marker = None
    #lock.acquire()
    markers = currentMarkers
    #lock.release()
    while marker is None:
        for i, m in enumerate(markers):
            if mid == m.mid:
                return m
        if(ur5 is not None):
            if(ur5.at_home == False):
                ur5.home()
        time.sleep(0.1)
        print "Marker " + str(mid) + " not found in fast markers. Waiting..."
        #lock.acquire()
        markers = currentMarkers
        #lock.release()

def euclidian_distance(p1, p2):
    dist = sqrt(pow(fabs(p1[0]-p2[0]),2)+pow(fabs(p1[1]-p2[1]),2))
    return dist

def inscribed_circle(p1, p2, p3):
    a = euclidian_distance(p1, p2)
    b = euclidian_distance(p1, p3)
    c = euclidian_distance(p2, p3)
    p = a+b+c
    k = 0.5*p
    r = (sqrt(k*(k-a)*(k-b)*(k-c)))/k
    ox = (a*p3[0]+b*p2[0]+c*p1[0])/p
    oy = (a*p3[1]+b*p2[1]+c*p1[1])/p
    return (r, (int(ox), int(oy)))

def get_vacant_position(markers, radius, pri_moves=None, future_pos=None):
    # TODO: the ordering is hardwired to the current corner marker layout
    # and should somehow be made general or configurable
    cnt_order = [markers[0], markers[2], markers[3], markers[1]]
    corners = np.array([[int(m.center()[0]), int(m.center()[1])] for m in cnt_order])
    print corners
    rect = cv2.boundingRect(corners)
    subdiv = cv2.Subdiv2D(rect)
    for m in markers:
        if pri_moves is not None and future_pos is not None and m.mid in pri_moves:
            real_xy = future_pos[m.mid][0]
            screen_xy = np.dot( np.linalg.inv(m.transform), [real_xy[0], real_xy[1], 1])
            #TODO: Normalize screen_xy 
            subdiv.insert((int(screen_xy[0]), int(screen_xy[1])))
        else:
            subdiv.insert(m.center())
    triangleList = subdiv.getTriangleList()
    for t in triangleList:
        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])
        if cv2.pointPolygonTest(corners, pt1, False) >= 0 and cv2.pointPolygonTest(
                corners, pt2, False) >= 0 and cv2.pointPolygonTest(corners, pt3, False) >= 0:
            r, incenter = inscribed_circle((t[0], t[1]), (t[2], t[3]), (t[4], t[5]))
            if r >= radius:
                # return pixel and robot coordinates of vacant position
                return (incenter, np.dot(markers[0].transform, [incenter[0], incenter[1], 1.0]))
    return None

_counter = 0

class TrackerFai(threading.Thread):
    
    def __init__(self, mid, transform, mid_aux = 0, video_source=1, capture=True, show=False, debug=False, useNewMarkers=False):
        threading.Thread.__init__(self)
        self.mid = mid
        self.mid_aux = mid_aux
        self.source = video_source
        self.transform = transform
        self.capture = capture
        self.show = show
        self.fps = 0
        self.lastMarkers=deque([])
        self.filterLen=8
        self.debug=debug
        self.height = -10
        self.originCalibrationMarkers = []
        self.useNewMarkers = useNewMarkers
        self.marker_trees = varying__single_layer_markers if useNewMarkers else small_markers
        
        cv2.setUseOptimized(True)

        print "starting Tracker, video source: ", self.source
        self.cap = cv2.VideoCapture(self.source)
        self.cap.set(3, 1920)#1920
        self.cap.set(4, 1080)#1080
        self.cap.set(37, 1) # turn the autofocus off, save and comment
        self.cap.set(cv2.CAP_PROP_FPS, 15)
#        for i in range(30):
        self.cap.read()
        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        #self.out = cv2.VideoWriter('output.avi', fourcc, 15, (1920, 1080), True)
        frame_width = int(self.cap.get(3))
        frame_height = int(self.cap.get(4))
        self.out = cv2.VideoWriter('outpy.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frame_width, frame_height))
 
    def stop(self):
        self.cap.release()
        self.out.release()

    def _get_marker_object(self, mid, markers):
        idx = -1
        marker = None
        for i, m in enumerate(markers):
            if mid == m.mid:
                idx = i
                marker = m
                break # return first, similar to index()
        return (idx, marker)
    
    def _getMarkers(self):
        global safeMarkers
        global image
        global currentMarkers
        ret, frame = self.cap.read()

        if self.debug == True:
            global _counter
            photos=0
            #dir = "../images/images_cal/markers/"
            dir = "../images/images_cal/testing/"
            for file in os.listdir(dir):
                if file.endswith(".jpg"):
                    #print(file)
                    photos+=1
            name = dir + str( _counter%photos)
            fullname = name + '.jpg'
            print "processing image ", fullname
            _counter += 1
            time.sleep(1)
            cv2.waitKey(0)
            frame = cv2.imread(fullname,cv2.IMREAD_COLOR)
        
        #undist = cv2.remap(frame, self.mapx, self.mapy, cv2.INTER_LINEAR)
        undist = cv2.undistort(frame, self.cmat2, cdist)
        #undist = cv2.pyrDown(undist)
        thresh = self.preprocess_image(undist)
        image =  undist
        cv2.imshow('blackandwhite', thresh)
        img2, contours, hierarchy = self.segmentation(thresh)

        marker_sizes = self.find_marker_sizes(self.marker_trees)
        if self.useNewMarkers:
            markers = self.find_markers_alt(contours, hierarchy, marker_sizes[0], marker_sizes.pop(), self.transform)
        else:
            markers = self.find_markers(contours, hierarchy, max(marker_sizes), self.transform)

        markers.sort(key=lambda m: m.mid)
        
        currentMarkersTmp = deepcopy(markers)
        self.handleDoubleMarkers(currentMarkersTmp)    
        lock.acquire()
        currentMarkers = currentMarkersTmp
        lock.release()

        if self.show:
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(image, str(round(self.fps, 2)), (10, 25), font, 1, (255, 255, 255), 2, cv2.LINE_AA)

            for m in markers:
                #cv2.circle(image, m.center(), 50, white, 3)
                cx, cy = m.center()
                center = (int(cx), int(cy))
                angle = m.orientation()
                l = 40
                pt2 = (int(round(cx + l * math.cos(angle))), int(round(cy + l * math.sin(angle))))
                #cv2.line(image, center, pt2, green, 2, cv2.LINE_AA)
                cv2.putText(image, str(m.mid), (cx + 30, cy + 30), font, 1, (0, 0, 255), 2, cv2.LINE_AA)
                #cv2.putText(image, str(angle), (int(round(cx)) + 45, int(round(cy)) + 45), font, 1, (255, 0, 0), 2,
                 #           cv2.LINE_AA)

            showSafeMarkers = False
            if (showSafeMarkers):
                for m in safeMarkers:
                    cx, cy = m.center()
                    center = (int(round(cx)), int(round(cy)))
                    if m.isMoving == False:
                        cv2.circle(image, center, 60, (255, 0, 0), 3)
                    else:
                        cv2.circle(image, center, 60, (255, 0, 0), 7)
                    angle = m.orientation()
                    l = 40
                    pt2 = (int(round(cx+l*math.cos(angle))), int(round(cy + l*math.sin(angle))))
                    cv2.line(image, center, pt2, (255, 0, 0), 2, cv2.LINE_AA)
                    #cv2.putText(image, str(m.mid), (int(round(cx)) + 45, int(round(cy)) + 45), font, 1, (255, 0, 0), 2, cv2.LINE_AA)
                    cv2.putText(image, str(angle), (int(round(cx)) + 45, int(round(cy)) + 45), font, 1, (255, 0, 0), 2,
                                cv2.LINE_AA)

            if (False):
                if m is not None:
                    cx, cy = m.center()
                    center = (int(round(cx)), int(round(cy)))
                    cv2.circle(image, center, 60, (0, 0, 255), 3)
                    angle = m.orientation()
                    l = 40
                    pt2 = (int(round(cx+l*math.cos(angle))), int(round(cy + l*math.sin(angle))))
                    cv2.line(image, center, pt2, (0, 0, 255), 2, cv2.LINE_AA)
                    cv2.putText(image, str(m.mid), (int(round(cx)) + 45, int(round(cy)) + 45), font, 1, (0, 0, 255), 2, cv2.LINE_AA)
            
            for m in self.originCalibrationMarkers:
                x, y = m.center()
                #cv2.circle(image, (int(x), int(y)), 4, purple, -1)
                
            cv2.imshow('capture', image)
            #cv2.waitKey(1)
        _counter += 1
        self.out.write(undist)
        if self.capture and _counter > 10:
            cv2.imwrite('raw.jpg', frame)
            cv2.imwrite('undist.jpg', undist)
            cv2.imwrite('thresh.jpg', thresh)
        if self.debug and self.capture:
            cv2.imwrite(name + ' - thresh.jpg', thresh)
            cv2.imwrite(name + ' - res.jpg', image)
        return markers

    def run(self):
        global lock
        global safeMarkers
        global performCalibration


        #Find the undistorsion parameters
        self.cmat2, self.roi = cv2.getOptimalNewCameraMatrix(cmat, cdist, (1920,1080), 0, (1920,1080))
        self.mapx, self.mapy = cv2.initUndistortRectifyMap(self.cmat2, cdist, None, self.cmat2, (1920,1080), 5)
        
        #Fill the markers array 
        for i in range(self.filterLen):
            m = self._getMarkers()
            self.lastMarkers.append(m)
        print "Last markers len: " + str(len(self.lastMarkers))
        
        
        while(True):
            
            start = time.time()
            m = self._getMarkers()
            #Add the last markers and remove the first one
            self.lastMarkers.append(m)
            self.lastMarkers.popleft()
            
            #filter the markers 
            filteredMarkers = self.filterMarkers()  
            
            lock.acquire()
            safeMarkers = filteredMarkers
            lock.release()
            end = time.time()
            self.fps = self.fps * 9/10 + 1/(10*(end-start))

            if (self.transform is None and self.areCornersDetected(filteredMarkers) == True):
                calibration.redo_transform(filteredMarkers)
                self.transform, _, self.height = calibration.restore()
                for i in range (4):
                    _, mTmp = self._get_marker_object(i+1, filteredMarkers)
                    self.originCalibrationMarkers.append(mTmp)
            
            if(performCalibration == True):
                print "Trying to calibrate"
                if self.areCornerMarkersWellPlaced(filteredMarkers) == True:
                    calibration.redo_transform(filteredMarkers)
                    self.transform, _, self.height = calibration.restore()
                    self.originCalibrationMarkers=[]
                    for i in range (4):
                        _, mTmp = self._get_marker_object(i+1, filteredMarkers)
                        self.originCalibrationMarkers.append(mTmp)
                    performCalibration = False

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        self.stop()


    def filterMarkers(self):
        
        tempM=[None]*13
        filteredMarkers=[]
        for i in range(len(self.lastMarkers)):
            for m in self.lastMarkers[i]:
                if m.mid < 13:
                    if tempM[m.mid] == None:
                        tempM[m.mid]=[]
                    tempM[m.mid].append(m)
               
        for i in range(13):
            if tempM[i] is not None:
                cxA = np.zeros(len(tempM[i]))
                cyA = np.zeros(len(tempM[i]))
                angleA1 = np.zeros(len(tempM[i]))
                angleA2 = np.zeros(len(tempM[i]))
                angleA3 = np.zeros(len(tempM[i]))
                angleA4 = np.zeros(len(tempM[i]))
                
                #Get the coordinates and the angle and store them in np arrays 
                for j in range(len(tempM[i])):
                    cx, cy = tempM[i][j].center()
                    cxA[j] = cx
                    cyA[j] = cy
                    orient = tempM[i][j].orientation()
                    a1, a2, a3, a4 = tempM[i][j].getAlternativeAngle()
                    angleA1[j] = a1
                    angleA2[j] = a2
                    angleA3[j] = a3
                    angleA4[j] = a4
#                cxM = np.mean(cxA)
#                cyM = np.mean(cyA)
#                angle1M = np.mean(angleA1)
#                angle2M = np.mean(angleA2)
#                angle3M = np.mean(angleA3)
#                angle4M = np.mean(angleA4)
                
                #Detect outliers
                data = [cxA, cyA, angleA1, angleA2, angleA3, angleA4]
                outliers = self.calculateOutliers(data)

                #Remove outliers
                cxA = np.delete(cxA, outliers)
                cyA = np.delete(cyA, outliers)
                angleA1 = np.delete(angleA1, outliers)
                angleA2 = np.delete(angleA2, outliers)
                angleA3 = np.delete(angleA3, outliers)
                angleA4 = np.delete(angleA4, outliers)
                               
                #Create the new Markers 
                if(len(cxA) > 0):
                    m = Marker(i, None, None, None, self.transform, self.height)
                    a1M = np.mean(angleA1)
                    a2M = np.mean(angleA2)
                    a3M = np.mean(angleA3)
                    a4M = np.mean(angleA4)
                    newAngle = Marker.angle_between_points(a1M, a2M, a3M, a4M)
                    m.updateMarker(np.mean(cxA), np.mean(cyA), newAngle)
                    
                    #detect movement
                    z = len(angleA1)-1
                    if((abs(angleA1[z] - angleA1[0]) > 3)  or (abs(angleA2[z] - angleA2[0]) > 3) or (abs(angleA3[z] - angleA3[0]) > 3)  or (abs(angleA4[z] - angleA4[0]) > 3)):
                        m.isMoving = True
                    
                    filteredMarkers.append(m)
                 

                
#                if i==1:
#                    print str(len(cxA)) + " " + str(len(outliers)) 
#                    print str(round(cxM,2)) + "  " +  str(round(cyM,2)) + "  " +  str(round(angleM,2))+ " == " + str(round(np.mean(cxA),2)) + " " + str(round(np.mean(cyA),2)) + " " + str(round(np.mean(angleA),2)) + " == " + str(len(outliers)/float(len(tempM[i]))*100 )
        
        self.handleDoubleMarkers(filteredMarkers)       
        return filteredMarkers

    def handleDoubleMarkers(self, markers):
        ###Hack to get the correct pos of the robot
        index5, m5 = self._get_marker_object(5, markers)
        if(m5 is not None):
            index6, m6 = self._get_marker_object(6, markers)
            if(m6 is not None):
                cx5, cy5 = m5.center()
                cx6, cy6 = m6.center()
                angle5 = m5.orientation()
                angle6 = m6.orientation()
                newAngle = Marker.angle_between_points(cx5, cy5, cx6, cy6)
                m5.updateMarker((cx5+cx6)/2, (cy5+cy6)/2, newAngle)
            else:
                #If we find only the marker 5 we cannot calcualte the position of the robot
                markers.pop(index5)
        
    def calculateOutliers(self, data):
        median = np.median(data, axis=1)
        iL = np.percentile(data, 25, axis=1)
        iH = np.percentile(data, 75, axis=1)
        
        outlierA=[]
        for i in range(len(data[0])):
            outlier=False
            for j in range(3):
                err = abs(data[j][i]-median[j])
                if(err > 1.5*(iH[j]-iL[j])):
                    outlier=True
            if(outlier):
                outlierA.append(i)
        return outlierA

    def preprocess_image(self, img, blur=False):

        use_changes = True
        if use_changes:
            img = cv2.bilateralFilter(img,5,100,100)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            th = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                    cv2.THRESH_BINARY ,37,4)
            erodeKernel = 3
            dilateKernel = 2
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (erodeKernel, erodeKernel))
            th = cv2.erode(th,kernel,iterations = 1)
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (dilateKernel, dilateKernel))
            th = cv2.dilate(th,kernel,iterations = 1)
            return th
        else:
            # ORIGINAL CODE
            #    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            #    if blur:
            #        blur = cv2.GaussianBlur(gray, (5, 5), 0)
            #        ret, th = cv2.threshold(blur, 0, 255,
            #                                cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            #    else:
            #        ret, th = cv2.threshold(gray, 0, 255,
            #                                cv2.THRESH_BINARY + cv2.THRESH_OTSU)

            #    img = cv2.medianBlur(img,3)
            img = cv2.bilateralFilter(img, 5, 100, 100)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            th = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                       cv2.THRESH_BINARY, 23, 3)
            kernel = np.ones((3, 3), np.uint8)
            th = cv2.erode(th, kernel, iterations=1)
            kernel = np.ones((2, 2), np.uint8)
            th = cv2.dilate(th, kernel, iterations=1)
            return th


    def segmentation(self, img):
        return cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    def find_markers(self, contours, hierarchy, size, transform=None):
        """Find and id markers in image contours

        Keyword arguments:
        contours  -- contours found in image using cv2.findContours
        hierarchy -- the contour hierarchy. Requires mode CV_RETR_TREE
        size      -- the fixed no. of tree nodes in a marker tree
        """
        tree = hierarchy[0]
        tsize = len(tree)
        matches = []
        for n in range(0, tsize):
            nxt, pre, chd, par = tree[n]
            if chd != -1:
                if n + size <= tsize:
                    (found, m) = self.id_marker(contours, tree, n, n + size)
                    if found:
                        m.transform = transform
                        matches.append(m)
        return matches

    def areCornersDetected(self, ms):
        repeat = True
        #print(len(ms))

        md = {}
        mu = {}
        index=0
        for m in ms:
            if (m.mid <= 4 and m.mid > 0):
                mu[index] = m
                md[m.mid] = m
                index +=1
                #print "Marker ", m.mid, " found"

        repeat = False
        if not md.has_key(1):
            #print("ERR: Marker 1 missing.")
            repeat = True
        if not md.has_key(2):
            #print("ERR: Marker 2 missing.")
            repeat = True
        if not md.has_key(3):
            #print("ERR: Marker 3 missing.")
            repeat = True
        if not md.has_key(4):
            #print("ERR: Marker 4 missing.")
            repeat = True
        if len(mu) is not 4:
            repeat = True

        return not repeat

    def areCornerMarkersWellPlaced(self, markers):

        if self.areCornersDetected(markers) == False:
            return False

        idx1, m1 = self._get_marker_object(1, markers)
        idx2, m2 = self._get_marker_object(2, markers)
        idx3, m3 = self._get_marker_object(3, markers)
        idx4, m4 = self._get_marker_object(4, markers)

        if(m1.realxy() is None):
            return False

        if(m1.realxy()[0] > 0.3 and m1.realxy()[0] < 0.2):
            return False
        if(m1.realxy()[1] > -0.45 and m1.realxy()[1] < -0.55):
            return False

        if(m2.realxy()[0] > 0.3 and m2.realxy()[0] < 0.2):
            return False
        if(m2.realxy()[1] < 0.45 and m2.realxy()[1] > 0.55):
            return False

        if(m3.realxy()[0] > 0.8 and m3.realxy()[0] < 0.7):
            return False
        if(m3.realxy()[1] < 0.45 and m3.realxy()[1] > 0.55):
            return False

        if(m4.realxy()[0] > 0.8 and m4.realxy()[0] < 0.7):
            return False
        if(m4.realxy()[1] > -0.45 and m4.realxy()[1] < -0.55):
            return False

        print "Corners: "
        print m1.realxy()
        print m2.realxy()
        print m3.realxy()
        print m4.realxy()
        return True

    def id_marker(self, contours, tree, start, end):
        """Return a Marker object if found in subtree

        Keyword arguments:
        contours -- contours found in image using cv2.findContours
        tree     -- hierarchy array (ie. hierarchy[0])
        start    -- start index
        end      -- end index
        """
        global image
        markerMinRadiusOffset = -25
        markerMaxRadiusOffset = 60

        radiusScalar = 5
        leafMaxRadius = 2 * radiusScalar
        markerMinRadius = 25 + markerMinRadiusOffset
        markerMaxRadius = 45 + markerMaxRadiusOffset
        seq = '0'
        sub, t = [], []
        l1, l2 = [], []
        depth = 1
        parent = [start]

        for n in range(start +1, end):
            if len(contours[n]) <= 2:
                return (False, None)
            nxt, pre, chd, par = tree[n]
            t.append(depth)
            if depth == 2:
                l2.append(contours[n])
            # Parent since child is next node - increment depth and put parent on stack
            if chd == n + 1:
                depth = depth + 1
                if depth > 2:
                    return (False, None)  # Immediately return on invalid depth
                parent.append(n)
            # Lonely node on depth one - singleton leave
            elif depth == 1 and chd == -1:
                sub.append(t)
                t = []
                l1.append(contours[n])
            # Last child of the last parent on stack - decrement depth
            # and start new sub-sequence
            elif nxt == -1 and par == parent[-1:][0]:
                depth = depth - 1
                sub.append(t)
                t = []
                parent.pop()

        # Process sub-sequences -
        # 1. sort by length in reverse for left-heavy ordering
        # 2. flatten sub-sequences
        # 3. join to string and append to seq
        if len(sub) < 3:
            return invalid_marker

        sub.sort(key=len, reverse=True)
        join = [n for s in sub for n in s]
        seq = seq + reduce(lambda x, y: x + y, map(str, join))

        if not self.marker_trees.has_key(seq):
            return invalid_marker

        #if self.marker_trees.has_key(seq):
        #Check ouside countour
        font = cv2.FONT_HERSHEY_SIMPLEX
        (xCenter,yCenter),radiusC = cv2.minEnclosingCircle(contours[start])
        outRadius = radiusC
        outX = xCenter
        outY = yCenter
        center = (int(xCenter),int(yCenter))
        radiusC = int(radiusC)
        #cv2.circle(image,center,radiusC,(255,0,255),1)
        #cv2.putText(image, str(radiusC), center, font, 1, (255,0,255), 2, cv2.LINE_AA)
        #Discard small ones and large ones
        if((radiusC > markerMaxRadius) or (radiusC < markerMinRadius)):
            return invalid_marker


        #Check leaves l1 and l2
        for leaf in range(len(l1)):
            (xC,yC),radiusC = cv2.minEnclosingCircle(l1[leaf])
            dist = math.sqrt ( (outX-xC)*(outX-xC) + (outY-yC)*(outY-yC))
            if((radiusC > leafMaxRadius) or dist > outRadius):
                return invalid_marker
            l1_center = (int(xC),int(yC))
            radiusC = int(radiusC)
            # Red circles
            #cv2.circle(image,center,radiusC,(0,0,255),-1)
            #cv2.putText(image, str(radiusC), center, font, 1, (0,0,255), 2, cv2.LINE_AA)

        for leaf in range(len(l2)):
            (xC,yC),radiusC = cv2.minEnclosingCircle(l2[leaf])
            dist = math.sqrt ( (outX-xC)*(outX-xC) + (outY-yC)*(outY-yC))
            if((radiusC > leafMaxRadius) or dist > outRadius):
                return (False, None)
            #center = (int(xC),int(yC))
            radiusC = int(radiusC)
            # Yellow circles
            #cv2.circle(image,center,radiusC,(0,255,255),-1)
            #cv2.putText(image, str(radiusC), center, font, 1, (0,255,255), 2, cv2.LINE_AA)

        if self.useNewMarkers:
            # First find top point
            (success, tp, index) = self.findTop(l1, False)
            if not success:
                return invalid_marker

            # Find outer image points on marker used for pnp
            (success, image_points) = self.find_marker_eight_points(contours[start], tp)
            if not success:
                return invalid_marker

            # Then find rotation vector and translation vector with pnp
            (success, rvec, tvec) = self.solve_pnp_iteratively(image_points)
            if not success:
                return invalid_marker

            # Find projected quadrant image points
            quadrant = self.find_quadrants(rvec, tvec, False)
            if not self.check_valid_quadrant(quadrant):
                return invalid_marker

            # Find eular angles
            eular = self.find_eular_angles(rvec, tvec)

            # Try and find if sequence exists
            try:
                variationSeq = self.findSequence(quadrant, l1, False)
                possible = self.marker_trees[seq]
                id = possible[variationSeq]
            except: return invalid_marker

            # Draw rotation axis except on corners
            if id != 1 and id != 2 and id != 3 and id != 4:
                # Currently the function does not return a correct answer. Implement in future
                #rx, ry, rz = self.translate_to_UR5_rotation(eular)
                self.draw_rotation_axis(rvec, tvec)

            m = SingleLayerMarker(id, contours[start], l1, l2, tp, eular, self.transform, self.height)
        else:
            id = self.marker_trees[seq]
            m = Marker(id, contours[start], l1, l2, self.transform, self.height)
        return (True, m)

    def translate_to_UR5_rotation(self, eular):
        #TODO The current implementation does not work. Also should not be in this class.
        baseX = 180
        rx = eular[1]
        ry = eular[2]
        rz = eular[0]
        rx = baseX + rx
        rz = (rz - 90) * -1
        ry = ry

        if ry <= 0 and ry >= -90:
            robRX = rx + rz
            robRZ = rz + (baseX - rx)
        elif ry <= -90 and ry >= -180:
            robRX = baseX + (baseX - rx) + rz
            robRZ = (baseX - rx) - rz
        elif ry <= 180 and ry >= 90:
            robRX = baseX + (baseX - rx) - rz
            robRZ = -(baseX - rx) - rz
        else:
            robRX = rx - rz
            robRZ = rz - (baseX - rx)

        return robRX, ry, robRZ

    def check_valid_quadrant(self, quadrant):
        min_dist = 10
        max_dist = 25
        min_dist_quadrants = 15
        (cp, tp, rp, bp, lp) = quadrant

        cpT = euclidian_distance(cp, tp)
        cpR = euclidian_distance(cp, rp)
        cpB = euclidian_distance(cp, rp)
        cpL = euclidian_distance(cp, rp)

        ttr = euclidian_distance(tp, rp)
        rtb = euclidian_distance(rp, bp)
        btl = euclidian_distance(bp, lp)
        ltt = euclidian_distance(lp, tp)

        # Check valid distance between quadrants
        if ttr < min_dist_quadrants or rtb < min_dist_quadrants or btl < min_dist_quadrants or ltt < min_dist_quadrants:
            return False

        # Check valid distance between quadrants and center
        for dist in [cpT, cpR, cpB, cpL]:
          if dist < min_dist or dist > max_dist:
              return False

        return True

    # Used to find the markers in the new system. 
    def find_markers_alt(self, contours, hierarchy, minSize, maxSize, transform=None):
        """Find and id markers in image contours

        Keyword arguments:
        contours  -- contours found in image using cv2.findContours
        hierarchy -- the contour hierarchy. Requires mode CV_RETR_TREE
        size      -- the fixed no. of tree nodes in a marker tree
        """
        tree = hierarchy[0]
        tsize = len(tree)
        matches = []

        for n in range(0, tsize):
            nxt, pre, chd, par = tree[n]
            if chd != -1:
                childCount = tsize - n if nxt == -1 else nxt - n
                if n + minSize <= tsize:
                    interval = min(maxSize, childCount)
                    interval = max(interval, minSize)
                    highest = interval if n + interval < tsize else tsize - n
                    for i in range(highest, minSize-1, -1):
                        (found, m) = self.id_marker(contours, tree, n, n + i)
                        if found:
                            m.transform = transform
                            matches.append(m)
                            break

        return matches

    def find_marker_eight_points(self, contours, tp):
        
        # Simplify marker contour down to eight points
        epsilon = 0.001 * cv2.arcLength(contours, True)
        approx = cv2.approxPolyDP(contours, epsilon, True)
        hull = cv2.convexHull(approx, True)
        epsilon = 0.01 * cv2.arcLength(hull, True)
        approx = cv2.approxPolyDP(hull, epsilon, True)
        final = approx
        if len(final) != 8:
            return invalid_marker

        # Remove false positives if outer marker not circular
        perimeter = cv2.arcLength(final, True)
        area = cv2.contourArea(final)
        circularity = 4 * math.pi * (area / (perimeter * perimeter))
        if circularity > 1 or circularity < 0.7:
            return invalid_marker

        # Find points closest to top-quadrant
        dists = [euclidian_distance(tp, (h[0][0], h[0][1])) for h in final]
        temp = sorted(dists)
        (index1, index2) = (dists.index(temp[0]), dists.index(temp[1]))
        list_size = len(final)
        offset = list_size - (max(index1, index2) if (index1 == 0 and index2 == list_size - 1) or (
                        index2 == 0 and index1 == list_size - 1) else min(index1, index2))
        actual_points = [None] * list_size
        for i in range(0, list_size):
            h = final[(i - offset) % list_size][0]
            actual_points[i] = (float(h[0]), float(h[1]))

        # Draw for debugging
        draw_image_points = False
        if draw_image_points:
            cv2.drawContours(image, [final], 0, (0, 255, 0), 2)
            colors = [(0, 0, 255), (0, 106, 255), (0, 216, 255),(255, 255, 0), (255, 148, 0), (255, 38, 0), (255, 0, 178), (255, 0, 220)]
            for i in range(0, len(actual_points)):
                (ax, ay) = actual_points[i]
                cv2.circle(image, (int(ax), int(ay)), 3, colors[i], -1)

        return (True, actual_points)

    def solve_pnp_iteratively(self, image_points):
        image_points = np.array(image_points)
        model_points = Marker.getMarkerModelPointsAlt()
        if len(model_points) != len(image_points):
            return (False, None, None)

        (success, rotation_vector, translation_vector) = cv2.solvePnP(model_points, image_points,
                                                                      cmat, cdist,
                                                                      None, None, False,
                                                                      flags=cv2.SOLVEPNP_ITERATIVE)
        return (success, rotation_vector, translation_vector)

    def find_quadrants(self, rotation_vector, translation_vector, draw_quadrant_points = False):
        (points, jacobian) = cv2.projectPoints(
            np.array([(0.0, 0.0, 0.0), (0.0, 0.0, 17.5), (17.5, 0.0, 0.0), (0.0, 0.0, -17.5), (-17.5, 0.0, 0.0)]),
            rotation_vector,
            translation_vector,
            cmat, cdist)

        cp = (int(points[0][0][0]), int(points[0][0][1]))
        tp = (int(points[1][0][0]), int(points[1][0][1]))
        rp = (int(points[2][0][0]), int(points[2][0][1]))
        bp = (int(points[3][0][0]), int(points[3][0][1]))
        lp = (int(points[4][0][0]), int(points[4][0][1]))

        if draw_quadrant_points:
            circle_size = 3
            cv2.circle(image, cp, circle_size, white, -1)
            cv2.circle(image, tp, circle_size, red, -1)
            cv2.circle(image, rp, circle_size, yellow, -1)
            cv2.circle(image, bp, circle_size, green, -1)
            cv2.circle(image, lp, circle_size, blue, -1)

        return (cp, tp, rp, bp, lp)


    def draw_rotation_axis(self, rotation_vector, translation_vector):

        # Find 2d points projected from 3d points given rotation and translation vector
        (points, jacobian) = cv2.projectPoints(
            np.array([(0.0, 0.0, 0.0), (50.0, 0.0, 0.0), (0.0, -50.0, 0.0), (0.0, 0.0, 50.0)]),
            rotation_vector,
            translation_vector,
            cmat, cdist)

        point_c = (int(points[0][0][0]), int(points[0][0][1]))
        point_x = (int(points[1][0][0]), int(points[1][0][1]))
        point_y = (int(points[2][0][0]), int(points[2][0][1]))
        point_z = (int(points[3][0][0]), int(points[3][0][1]))

        cv2.line(image, point_c, point_x, red, 3)
        cv2.line(image, point_c, point_z, blue, 3)
        cv2.line(image, point_c, point_y, green, 3)

    def find_eular_angles(self, rotation_vector, translation_vector):

        # Find rotation matrix
        rvec_matrix, _ = cv2.Rodrigues(rotation_vector)
        proj_matrix = np.hstack((rvec_matrix, translation_vector))
        eulerAngles = cv2.decomposeProjectionMatrix(proj_matrix)[6]
        pitch = eulerAngles[0]  # Y-axis
        yaw = eulerAngles[1] # Z-axis
        roll = eulerAngles[2] # Z-axis

        return pitch, yaw, roll

    def findSequence(self, quadrant, l1, drawSequence = False):
        (cx, cy), (tx, ty), rp, bp, lp = quadrant

        # Find contours closest to each quadrant
        l = []
        b = []
        r = []

        points = [(cx, cy), (tx, ty), rp, bp, lp]
        for c in l1:
            cPoint, _ = cv2.minEnclosingCircle(c)
            clstP = self.findNearestPoint(cPoint, points)

            if clstP == lp:
                l.append(c)
                color = blue
                if drawSequence:
                    cv2.drawContours(image, c, -1, color, 3)
            elif clstP == bp:
                b.append(c)
                color = green
                if drawSequence:
                    cv2.drawContours(image, c, -1, color, 3)
            elif clstP == rp:
                r.append(c)
                color = yellow
                if drawSequence:
                    cv2.drawContours(image, c, -1, color, 3)

        # Varied sequences read l before b before r
        seq = len(l) * 'l' + len(b) * 'b' + len(r) * 'r'
        return ''.join(seq)

    # Finds and returns closest point to p
    def findNearestPoint(self, p, points):
        smlDist = float("inf")
        i = 0
        smlIndex = -1
        for nxtP in points:
            newDist = euclidian_distance(p, nxtP)
            if (newDist < smlDist):
                smlIndex = i
                smlDist = newDist
            i += 1
        return points[smlIndex]

    def findTop(self, l1, drawTop = False):
        min_radius = float('inf')
        max_radius = float('-inf')
        index = -1
        for i in range(0, len(l1)):
            center, radius = cv2.minEnclosingCircle(l1[i])
            radius = int(round(radius))

            min_radius = radius if radius < min_radius else min_radius
            if radius > max_radius:
                max_radius = radius
                largest_center = center
                index = i

        success = max_radius > min_radius*2 and max_radius < min_radius * 10

        if drawTop and success:
            cv2.drawContours(image, l1[index], -1, (0, 255, 0), 3)

        return (success, largest_center, index)

    def findMin(self, list):
        index = 0
        min = 10000000000
        for i in range(0, len(list)):
            if list[i] <= min:
                min = list[i]
                index = i
        return list[index], index

    def find_marker_sizes(self, m_list):
        sizes = set([])
        for m in m_list:
            sizes.add(len(m))

        sizes = sorted(sizes)
        return sizes