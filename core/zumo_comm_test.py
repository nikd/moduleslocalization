import serial
import sys
import time

def main(packets=50, port='/dev/tty.usbserial-FTFDEXW8', baudrate=115200):
    try:
        ser = serial.Serial(port, baudrate)
    except serial.SerialException as e:
        sys.exit("Invalid tty device")

    ser.write('R\n')
    time.sleep(0.5)
    ser.write('S\n')
    time.sleep(0.1)

    for i in range(packets):
        line = ser.readline()
        if line.startswith('I'):
            print("got input: "+line)
            inputs = line.strip().split(' ')
            ser.write('O,'+inputs[1]+',100,-100\n')
    time.sleep(0.1)
    ser.write('R\n')
    time.sleep(0.5)
    ser.close()

if __name__ == "__main__":
    args = sys.argv[1:]
    main(*args)
