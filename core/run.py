from tasks.TestBuildTask import TestBuildTask

from tasks.EnduranceTestTask import EnduranceTestTask
from tasks.StraightToTargetTask import StraightToTargetTask
from tasks.ObjectAvoidanceTask import ObjectAvoidanceTask
from tasks.DummyTask import DummyTask
from vision.TrackerFai import TrackerFai, get_markers, get_marker_object
import vision
import sys

def main(ttype, pop, gen, re):
    mid= 5
    transform=None
    mid_aux = 0
    video_source=0
    capture=False
    show=True
    thread1 = TrackerFai(mid, transform, mid_aux , video_source, capture, show)
    # Start new Threads
    thread1.start()
    
    itype = int(ttype) if ttype is not None else 1
    pop_size = int(pop) if pop is not None else 15
    gens = int(gen) if gen is not None else 20
    res = True if re == 'yes' else False
    print "pop_size: ", pop_size
    print "gens: ", gens
    print "re: ", re, ", res: ", res
    
    nExp = 3
    for n in range(nExp):
        if itype == 1:
            t = EnduranceTestTask()
        elif itype == 2:
            t = StraightToTargetTask(population_size=pop_size, generations=gens, resume=res, folder=str(n))
        elif itype == 3:
            t = ObjectAvoidanceTask(population_size=pop_size, generations=gens, resume=res)
        elif itype == 4:
            t = DummyTask(population_size=pop_size, generations=gens, resume=res)
        else:
            sys.exit(-1)
        t.run()

if __name__ == '__main__':
    args = sys.argv[1:]
    main(*args)
