import sys
import csv
import pickle
from math import fabs
import matplotlib.pyplot as plt
import numpy as np

def interval_map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def main(datafile):
    with open(datafile, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        reader.next()
        total = 0
        errs = [[], [], []]
        points = []
        for row in reader:
            vals = [float(s) for s in row]
            points.append(vals)
            total += 1
            errs[0].append(fabs(vals[0]-vals[3]))
            errs[1].append(fabs(vals[1]-vals[4]))
            errs[2].append(fabs(vals[2]-vals[5]))
    print('total_no_points: '+str(total))
    xerrs = np.array(errs[0])
    yerrs = np.array(errs[1])
    oerrs = np.array(errs[2])
    xysum = np.array([sum(a) for  a in zip(errs[0], errs[1])])
    xmean = xerrs.mean()
    xstd = xerrs.std()
    print('x_mean: '+str(xmean)+' y_mean: '+str(yerrs.mean())+' o_mean: '+str(oerrs.mean()))
    print('x_std: '+str(xstd)+' y_std: '+str(yerrs.std())+' o_std: '+str(oerrs.std()))
    print('x min/max: '+str(min(errs[0]))+'/'+str(max(errs[0])))
    print('y min/max: '+str(min(errs[1]))+'/'+str(max(errs[1])))
    print('o min/max: '+str(min(errs[2]))+'/'+str(max(errs[2])))
    cmin = min(xysum)
    cmax = max(xysum)
    omin = min(oerrs)
    omax = max(oerrs)
    for p,xye,oe in zip(points,xysum,oerrs):
        c = str(interval_map(oe, omin, omax, 1.0, 0.0))
        m = int(interval_map(xye, cmin, cmax, 4.0, 18.0))
        plt.plot(p[1], p[0], 'o', color=c, markersize=m)
    plt.title('EXP#1. Survey')
    plt.xlabel('real-world Y-axis')
    plt.ylabel('real-world X-axis')
    plt.axis([-0.5, 0.5, 0.75, 0.25])
    plt.show()

if __name__ == '__main__':
    args = sys.argv[1:]
    main(*args)
