import sys
import pickle
import matplotlib.pyplot as plt

def interval_map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def main(folder="", nofiles=7, prefix='best_positions_'):
    if nofiles is not 20:
        nofiles = int(nofiles)
    files = [(folder+prefix+str(n),n+1) for n in range(nofiles)]
    print('Reading '+str(len(files))+ 'files...')
    for f,g in files:
        with open(f, 'r') as fh:
            ps = pickle.load(fh)
            xs = [x for x,y,z in ps]
            ys = [y for x,y,z in ps]
            gs = interval_map(g, 1, nofiles, 1.0, .2)
            plt.plot(ys,xs,color=str(gs), linewidth=1.5)
            # plt.plot(ys,xs,linewidth=1.5)
    plt.plot(-0.03, 0.63, 'rx', markersize=12)
    plt.plot(0.15, 0.35, 'rx', markersize=12)
    plt.plot(0.4, 0.6, 'b<', markersize=12)
    plt.plot(-0.4, 0.58, 'go', markersize=12)
    plt.text(-0.41, 0.61, 'Target')
    plt.text(0.41, 0.58, 'Start')
    plt.title('EVO#2. Path from best genome in each generation')
    plt.xlabel('real-world Y-axis')
    plt.ylabel('real-world X-axis')
    plt.axis([-0.5, 0.5, 0.75, 0.25])
    plt.show()


if __name__ == '__main__':
    args = sys.argv[1:]
    main(*args)
