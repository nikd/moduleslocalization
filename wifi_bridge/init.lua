-- init.lua
wifi.setmode(wifi.STATION)

--modify according your wireless router settings
wifi.sta.config("Saturn","pr41s34S&LW!F!")
wifi.sta.connect()

function startup()
  tmr.stop(2)
  if abort == true then
    print('Startup aborted')
    return
  end
  uart.setup(0, 115200, 8, 0, 1, 1)
  dofile("main.lua")
end

abort = false
dhcp_count = 10

tmr.alarm(1, 1000, 1, function() 
    if wifi.sta.getip() == nil then
        print("IP unavailable, Waiting...")
        dhcp_count = dhcp_count - 1
        if dhcp_count == 0 then
          print("DHCP retries exceeded")
          tmr.stop(1)
        end
    else 
        tmr.stop(1)
        print("Config done, IP is "..wifi.sta.getip())        
        tmr.alarm(2, 5000, 0, startup)
    end 
end)
