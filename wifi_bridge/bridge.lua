-- Bridge.lua
-- Based loosely on code by Thorsten von Eicken, esp8266-lua-bridge , (C) 2015

uartConn = nil -- current connection that uart input goes to

ser2net = net.createServer(net.TCP, 60)
ser2net:listen(23, function(conn)
    if uartConn then 
      uartConn:close() 
    else
      uartConn = conn
    end

  conn:on("sent",function(conn)
    collectgarbage()
  end)
    
  -- Disconnection
  conn:on("disconnection", function(conn)
    uartConn:close() 
    collectgarbage()
  end)

  -- Reconnection
  conn:on("reconnection", function(conn)
    if uartConn then 
      uartConn:close() 
    else
      uartConn = conn
    end
  end)

  -- UART receive, TCP send
  uart.on("data", 0, function(data) 
    conn:send(data)
  end, 0)

  -- TCP receive, UART send
  conn:on("receive", function(conn, data) 
    uart.write(0, data)
  end)

end)