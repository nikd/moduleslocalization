#include <Wire.h>
#include <Zumo32U4.h>

//#define DEBUG 1
#define INTERVAL 200
#define STALL_MIN_DELTA 10
#define BUFFER_SIZE 80
#define SPEED_FWD 130
#define SPEED_TURN 80

LSM303 compass;
Zumo32U4Encoders encoders;
Zumo32U4Motors motors;
Zumo32U4ProximitySensors proxSensors;
Zumo32U4Buzzer buzzer;
Zumo32U4ButtonC buttonC;

bool output_ack = true;
bool is_running = false;
int step_count = 0;
LSM303::vector<int16_t> running_min = {32767, 32767, 32767}, running_max = {-32768, -32768, -32768};
char inbuf[BUFFER_SIZE];
int serIndex = 0;
const char encoderErrorLeft[] PROGMEM = "!<c2";
const char encoderErrorRight[] PROGMEM = "!<e2";
//int16_t countsLeft = STALL_MIN_DELTA; 
//int16_t countsLeftPrev = 0;
//int16_t countsRight = STALL_MIN_DELTA;
//int16_t countsRightPrev = 0;

void setup() {
  // Enable peripherals
#ifdef DEBUG
  Serial.begin(57600);
  while (! Serial);
  Serial.println("..--Domo, arigato, Mr. Roboto--..");
#endif
  Serial1.begin(9600);
  buttonC.waitForButton();
  
  Wire.begin();
  /* Initialize compass -
   * Calibration coefficients must match specific device
   */
  compass.init();
  compass.enableDefault();
  calibrateCompass();

  //Initialize remaining sensors
  proxSensors.initThreeSensors();
  /* After setting up the proximity sensors with one of the
   * methods above, you can also customize their operation: */
  proxSensors.setPeriod(300);
  proxSensors.setPulseOnTimeUs(421);
  proxSensors.setPulseOffTimeUs(900);
  uint16_t levels[] = { 2, 4, 6, 8, 10, 13, 18, 26, 32 ,38};
  proxSensors.setBrightnessLevels(levels, sizeof(levels)/2);

  // start line sensor calibration
  delay(1000);

#ifdef DEBUG
  Serial.println("Robot initialized");
#endif
}

void beep(byte n) {
  for (byte i = 0; i < n; i++) { 
    buzzer.playFrequency(440, 200, 10);
    delay(500);
  }
}

void calibrateCompass() {
  beep(1);
  ledYellow(1);
#ifdef DEBUG
  Serial.println("Compass calibration started. Move around!");
#endif
  unsigned long start = millis();
  while (millis() - start < 7000) {
    compass.read();
    
    running_min.x = min(running_min.x, compass.m.x);
    running_min.y = min(running_min.y, compass.m.y);
    running_min.z = min(running_min.z, compass.m.z);
  
    running_max.x = max(running_max.x, compass.m.x);
    running_max.y = max(running_max.y, compass.m.y);
    running_max.z = max(running_max.z, compass.m.z);
  }
  compass.m_min = running_min;
  compass.m_max = running_max;
#ifdef DEBUG
  snprintf(inbuf, BUFFER_SIZE, "min: {%+6d, %+6d, %+6d}    max: {%+6d, %+6d, %+6d}",
    running_min.x, running_min.y, running_min.z,
    running_max.x, running_max.y, running_max.z);
  Serial.println(inbuf);
#endif
  beep(2);
  ledYellow(0);
}

void readSensors() {
  proxSensors.read();
  compass.read();
}

void sendInputs() {
  char buf[80];
  // only send if previous transmission resulted in timely output
  if (output_ack) {
    step_count++;
    sprintf(buf, "I %d %d %d %d %d %d %d %d\n", 
    step_count, 
    (int)compass.heading(), 
    proxSensors.countsLeftWithLeftLeds(),
    proxSensors.countsLeftWithRightLeds(),
    proxSensors.countsFrontWithLeftLeds(),
    proxSensors.countsFrontWithRightLeds(),
    proxSensors.countsRightWithLeftLeds(),
    proxSensors.countsRightWithRightLeds());
    Serial1.print(buf);
#ifdef DEBUG
    Serial.println("Input sent");
#endif
    output_ack = false;
  }
#ifdef DEBUG
  else {
    Serial.print("Skipping send. No ack received for message #");
    Serial.println(step_count);
  }
#endif
}

void reset() {
  output_ack = true;
  is_running = false;
  step_count = 0;
  encoders.getCountsAndResetLeft();
  encoders.getCountsAndResetRight();
//  countsRight = STALL_MIN_DELTA;
//  countsLeft = STALL_MIN_DELTA;
//  countsRightPrev = 0;
//  countsLeftPrev = 0;
  readSensors();
}

void parseControl() {
  if (checkSerial()) {
    switch (inbuf[0]) {
      case 'O':
        {
          int ack;
          int speedleft;
          int speedright;
          sscanf(inbuf, "O,%d,%d,%d", &ack, &speedleft, &speedright);
#ifdef DEBUG
          Serial.println("-- OUTPUT");
#endif
          if (ack == step_count) {
            output_ack = true;
//            if(isStalling()) {
//              motors.setSpeeds(0, 0);
//            }
//            else {
              motors.setSpeeds(speedleft, speedright);
//            }
          }
#ifdef DEBUG
          else {
            Serial.println("ERR: ack/count mismatch");
          }
#endif
          break;
        }
      case 'F':
        {
          int ack;
          int spd;
          int dly;
          sscanf(inbuf, "F,%d,%d,%d", &ack, &spd, &dly);
          if (ack == step_count) {
            output_ack = true;
            motors.setSpeeds(spd, spd);
            delay(dly);
            motors.setSpeeds(0, 0);
          }
          break;
        }
      case 'T':
        {
          int ack;
          int dir;
          int spd;
          int dly;
          sscanf(inbuf, "T,%d,%d,%d,%d", &ack, &dir, &spd, &dly);
          if (ack == step_count) {
            output_ack = true;
            if(dir == 0) {
              motors.setSpeeds(-spd, spd);
            }
            else {
              motors.setSpeeds(spd, -spd);
            }
            dly = (int) (1.5*dly);
            delay(dly);
            motors.setSpeeds(0, 0);
          }
          break;
        }
      case 'R':
#ifdef DEBUG
        Serial.println("-- RESET");
#endif
        if (is_running) {
          motors.setSpeeds(0, 0);
          reset();
        }
        break;
      case 'S':
#ifdef DEBUG
        Serial.println("-- START");
#endif
        if (! is_running) {
          is_running = true;
        }
        break;
      case 'W':  
        output_ack=true;
        readSensors();
        sendInputs();
        break;
      case 'Z':  
        motors.setSpeeds(120, 120);
        delay(180);
        motors.setSpeeds(0, 0);
        output_ack=true;
        delay(200);
        readSensors();
        sendInputs();
        break;
    }
  }
}

bool checkSerial() {
  bool linefound = false;
  while (Serial1.available() > 0) {
    char cbuf = Serial1.read();
    if (cbuf == '\n') {
      inbuf[serIndex] = '\0';
      linefound = serIndex > 0;
      serIndex = 0;
      break;
    }
    else if (serIndex < BUFFER_SIZE && !linefound) {
      inbuf[serIndex++] = cbuf;
    }
  }
  return linefound;
}

//bool isStalling() {
//  countsLeft = encoders.getCountsLeft();
//  countsRight = encoders.getCountsRight();
//  int16_t delta = abs(countsLeft - countsLeftPrev) + abs(countsRight - countsRightPrev);
//  countsRightPrev = countsRight;
//  countsLeftPrev = countsLeft;
//  return (delta < STALL_MIN_DELTA);
//}

void loop() {
  static unsigned long prev_update = 0;
  unsigned long now = millis();

//  if (now - prev_update > INTERVAL && is_running) {
//    prev_update = now;
//    readSensors();
//    sendInputs();
//  }

  parseControl();
}

